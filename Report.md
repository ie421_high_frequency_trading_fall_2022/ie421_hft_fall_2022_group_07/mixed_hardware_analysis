# **Mixed Hardware Analysis**
## **Team Members**
**John Hofmann** - johnwh3@illinois.edu     
* John is a Bachelor's student ad the University of Illinois at Urbana-Champaign in the Engineering Department majoring in Computer Engineering. John is graduating Dec 2024.     
* Linkedin: https://www.linkedin.com/in/johnhofmanneng/

**Rahul Gupta** - rahulag2@illinois.edu   
* Rahul is a student at the University of Illinois Urbana-Champaign majoring in Computer Science and is graduating in May 2024.  
* Linkedin: https://www.linkedin.com/in/rahulguptavh/

**Ruidi Huang** - ruidih2@illinois.edu   
* Ruidi is currently pursuing a Computer Science degree at the University of Illinois Urbana-Champaign as an undergraduate student and is scheduled to graduate in May of 2024.
* Linkedin: https://www.linkedin.com/in/ruidi-huang/

**Yun (Tiger) Wang** - yunw3@illinois.edu   
* Tiger is a junior in Computer Science + Math at the University of Illinois Urbana-Champaign, graduating in May 2024.
* Linkedin: https://www.linkedin.com/in/tiger-wang-348054174/

## **Overview**
In this project, we explored the application of machine learning in algorithmic trading with an emphasis on the performance of different hardware accelerators and frameworks. We implemented a model that trains off of level two IEX market data on the SPY etf. The model’s architecture comes from the DeepLOB paper by Zihao Zheng, Stefan Zohren, and Stephen Roberts. We implemented the model in Pytorch, Tensorflow, and MXNet, and benchmarked their performance on Nvidia GPUs, a Google TPU, and Apple Silicon Macbooks on a variety of platforms. We hope you find this information useful when deciding how to train and deploy trading models.

### Usage

#### Requried Software

This project only requires [Vagrant](https://www.vagrantup.com), all the dependencies to run the code will be installed inside the container.

#### Installation

```bash
git clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2022/ie421_hft_fall_2022_group_07/mixed_hardware_analysis.git
cd mixed_hardware_analysis
vagrant up
```

#### Running Benchmarks

Run
```bash
vagrant ssh
```

Then when you are connected to the VM, run 
```bash
/vagrant/setup/scripts/startup.sh
```
and the benchmarks will start on your machine. To modify CPU/GPU execution and which frameworks to test, edit the command in /setup/scripts/startup.sh.

If there are any issues with packages or dependencies, run 
```bash
vagrant provision
```
on the host machine to reinstall the required libraries.

## **Model**
The model we use is based on [this paper]. It introduces a novel convolution-LSTM approach to extract features from the noisy stock market data. The extracted features are then passed through a convolution-pooling Inception module to further refine them before making a prediction on the market trend. The final prediction is made by passing the refined features through a fully connected linear layer.

The convolution-LSTM approach combines the strengths of both CNNs and LSTMs to effectively handle the high dimensional and sequential nature of stock market data. By treating the limit order book data as pages of images, CNNs can extract features while LSTMs can handle the sequential aspect. This approach requires no human engineering and can handle billions of data points seamlessly. Compared to traditional Markov models, the convolution-LSTM is believed to capture hidden data more effectively.

![](ReportImages/modeldiagram.png)

The Inception Module is a type of CNN that reduces the number of parameters and increases performance by using multiple convolutional filters of different sizes on the same input, and concatenating their outputs. This allows the model to learn multiple representations of the input, making it more robust to changes. A Maxpool layer is added to further reduce the dimensions of the feature maps and improve the model's performance. The Inception Module can be considered using different moving averages with different decay weights in technical analysis.


Based on the paper, we collect raw data that has the first twentieth asks and bids, then normalize them to fit into a normal distribution based on data from the previous five days. So, for each trade we have forty dimensions of data. 

To calculate the input features, we simply concatenate one hundred of those forty-dimensional data. To calculate the labels, we compare the average of the previous one hundred mid prices ((best bid + best ask) / 2) to the future one hundred mid prices. 


## **Nvidia GPUs**
Nvidia GPUs are currently the standard in machine learning and the cloud GPU market, and are also the only GPUs we recommend you buy and use. One of the main reasons is the maturity of their CUDA API and surrounding libraries. CuDNN (CUDA Deep Neural Network Library) is the library machine learning frameworks interface with, and using modern machine learning frameworks with Nvidia GPUs, and is about as plug and play as it gets.
### **GPU Classes**
The current Nvidia lines relevant to machine learning and trading algorithms are their gaming line consisting of the RTX 30 series and RTX 40 series, their Professional line consisting of the Quadro line and RTX A6000, their Design and Research line consisting of the Titan RTX, and the Datacenter line consisting of the Tesla series of GPUs. The types of compute “units” Nvidia GPUs have are CUDA cores, Tensor Cores, and RT Cores. CUDA cores are general processing compute units and are the most common type of compute unit on an Nvidia card. Tensor Cores are matrix multiply-accumulate accelerators and are what allow for automatic mixed precision training for machine learning. During mixed precision training, Tensor cores take in fp16 values to do the multiplication and then accumulate into fp32 values, allowing for faster model training. However, newer Tensor core generations allow for higher precision numeric types. 

![](ReportImages/archchart.png)

RT cores are ray tracing units, used to accelerate graphics and rendering tasks, and are mostly irrelevant to the kind of ml models one would run for trading. In fact, RT cores are not included in A100 and A30 GPUs, as they are focused on machine learning and high-performance computing.
### **A Note on Naming Conventions**
Nvidia products with RTX in the name have ray-tracing cores in them. Currently, GeForce cards are named by their series (20 series, 30 series, 40 series), followed by which tier they are in the series, where higher numbers are more powerful (3090 has more compute power than a 3050). If a gaming GPU has a Ti after its name, it occupies the space between the number above it and the model without the Ti (3080Ti is more powerful that a 3080 but less powerful than a 3090). Generally, if there is a letter before the model number, it refers to the architecture of the chip, H for Hopper (not out yet), A for Ampere, T for Turing, V for Volta, P for Pascal, M for Maxwell and K for Kepler. And generally, if two GPUs are in the same series or architecture, the one with the bigger number is more powerful.
### **Gaming GPUs**
The two current GeForce lines are the RTX 30 series and the RTX 40 series. These are the cards typically found in consumer laptops and desktop gaming PCs. The 30 series use a Samsung-manufactured 8nm Ampere architecture and Nvidia's second-generation ray tracing units and third-generation tensor cores. The pricing of the 30 series ranges from $250 for a 3050 to $1100 for a 3090 Ti. The 40 series use a TSMC-manufactured 4N 5nm Ada Lovelace architecture and Nvidia's third-generation ray tracing units and fourth-generation tensor cores. Currently, the 40 series only consists of the 4080 and 4090, with the 4070 Ti announced coming January 2020. The retail price of a 4080 is $1200 and for a 4090, $1600. Because these cards are targeted at the retail gaming consumer, the cards tend to have less VRAM than what may be desired. Therefore we recommend you avoid the 3070 Ti, 3070, and 3060Ti. The 3060 seems to be the best value for deep learning at its retail price point of $330, and 12GB of VRAM. If you need to own the hardware you are running on and have a budget below $2000 we recommend going with one of the higher-end 30 or 40-series GPUs, as these are really the only GPUs in this price category that have the benefits of the Nvidia ecosystem.

![](ReportImages/30Series.png)
![](ReportImages/40Series.png)

### **Professional GPUs**
The professional line was originally called Quadro, but since the switch to Ampere architectures, the only professional GPU sold by Nvidia is the RTX A6000, with the RTX 6000 Ada Generation announced and coming soon. The A6000 retails for $4650 and contains 48 GB GDDR6 memory, 10752 CUDA cores, 336 third-generation tensor cores, and 84 second-generation RT cores. However, while the RTX 4090 has less memory, it has more CUDA cores (16384), more Tensor Cores (512), and more RT cores (128). So if you want to buy your hardware, the choice between the 3090 and A6000 is whether you need the 48GB of contiguous memory, as the compute capability will likely be close to the same, although we didn’t get the chance to test either card. It is also worth noting you can buy two 4090s for less than the price of an A6000, assuming you would be paying the retail price for both. RTX A6000 can also be used in Lambda Labs GPU Cloud for $0.80 per hour.
### **Design and Research GPUs**
In their design and research class, Nvidia currently only sells the Titan RTX, which retails for $2500. The Titan RTX is a TSMC-manufactured 14nm Turing architecture, with 24 GB GDDR6 memory, 4608 CUDA cores, 576 second-generation Tensor Cores, and 72 RT cores. Similar to the A6000, you are getting a better deal by going for multiple gaming series GPUS, although we also didn’t get to benchmark a Titan either.
### **Data Center**
Nvidia's Tesla line of GPUs is typically what you will find on any cloud platform. Some of the most common being the A100, V100, T4, P4, and P100.

![](ReportImages/GCPofferings.png)

Google Cloud Platforms GPU offerings

We found the performance of these cards to be somewhat inline with their pricing, but not entirely. Here are general ranges of the on demand prices of these GPUs as of late 2022, although a more comprehensive and updated list can be found here (https://github.com/full-stack-deep-learning/website/blob/main/docs/cloud-gpus/cloud-gpus.csv).

![](ReportImages/pricingchart.png)
![](ReportImages/GPUtraining.png)

Here we compared the times of the more common data center GPUs, using a Tensorflow model on VMs on Google Cloud Platforms.  As far as training times go, the higher priced GPUs all ran faster than the cheaper ones; the faster cards beat all the cards cheaper than them. We also see the A100 and V100 massively outperform the rest.

![](ReportImages/GPUinference.png)

When it comes to inference time however the T4 performed better than the P100, and the rest of the cards were ordered by their price. However the differences during inference are much less significant than they were training, the V100 trains roughly 2-3 times faster than the T4, but has nearly the same inference time.

### **NGC Environments**
Nvidia GPU Cloud platform (NGC) provides GPU-optimized software all pre-installed, configured, and tested to run on their GPUs. Nvidia offers optimized docker environments for Pytorch, Tensorflow, Kaldi, MXNet, Tensor RT, and Paddle Paddle, which can be easily pulled and run on local or cloud machines. These also can easily be deployed to Google Vertex AI notebooks and used as a more feature-packed and powerful version of Google Colab.

### **TensorRT**
Within an NGC container you can convert a Pytorch, Tensorflow, or Matlab model into a TensorRT model. It is also worth noting that you can convert ONNX models into TensorRT, ONNX being the Open Neural Network Exchange, an open standard that allows the conversion between model types, that supports nearly all sizable machine learning frameworks. TensorRT is an SDK for high-performance inference and is known to accelerate inference time greatly.

### **Mixed Precision**
Setting up mixed precision is relatively easy, and depending on your framework will only require you to add a few lines to your code. In our tests, there was no significant difference between the training time of our standard and mixed models, although many developers have had very impressive model training time improvements with this feature.

### **Pricing and Recommendations**
When considering a GPU the most important feature is probably the amount of memory it has. Having superfast cores doesn’t matter if your model is too big to train, and crashes your runtime. After that, compute performance is probably the next most important, the number of CUDA and Tensor Cores is what you should care about. Some other things to consider are compatibility and longevity. Make sure the card you buy has the ability to use the features you want (this is why we recommend entirely against buying AMD GPUs).

The first thing you have to decide is whether you need your hardware or not. If you do, then it depends on your budget. For anything under $5000 you are best off sticking to gaming GPUs unless you absolutely need the 48 GB of memory the A6000 has. As mentioned before, you can buy two 4090s for less than the price of the A6000. We generally advise against buying used cards as there’s a pretty high chance you get a very used crypto miners card, however, if you are really on a tight budget, K80s are worth considering. You can buy a renewed K80 with 24 GB of memory for around $200 (It’s worth mentioning that K80s have two chips per card each with 12 GB of memory, so to use the full power of these cards you will have to use distribution strategies or have other ways of splitting models/model training), but if your budget is a little higher ($350) we still recommend the 3060. It's also worth mentioning that you need to include the price of all the hardware auxiliary to the GPU in your budgeting. If your budget is above $5000, you should probably start looking into the Data Center cards, although we are much less familiar with the process of buying higher-end hardware. 

If you don’t have a strong preference towards owning hardware or cloud deployments, from a price standpoint it can make sense to own your hardware, as we can assume you will be running a trading model during either standard market hours, futures market hours, or 24/7. Even using a cloud GPU that only costs a dollar per hour, running the 6.5 hours the market is open every day, will cost $130 per month, and if you are trading something 24/7 like crypto that's $720 per month, so just from a price of hardware standpoint it can easily make sense to run your own, however, you forgo the primary benefit you would get from a cloud deployment: you don’t have to manage your own hardware. Your trading also won’t crash whenever your internet goes down, or you lose power. One plus though is that inference is a much less intensive task than training, thus you may be able to use cheaper accelerators in your model deployment. As far as training goes, we tend to view faster training as more of a quality-of-life investment rather than necessarily one, although if your models are very large and take a long time to train it does make sense to invest heavily in this area. Of course, if you want the best training and inference times, an A100 is the best GPU you can get. Generally for cloud training and inference, the T4 stands out as a good value option, as its inference isn’t significantly slower than a V100, or even an A100, and its price is competitive compared to the V100 and A100. We didn’t test any multi-GPU configurations, but those are also one of the benefits of cloud training.

## **AMD GPUs**
AMD now has an API similar to CUDA called ROCm, although it is nowhere near as mature. On the machine learning end, they now have versions of Pytorch and Tensorflow with ROCm on the backend, as well as several other frameworks. However this support is limited to a small selection of their GPUs (their Vega series and the Navi 2X series) and from what I can tell from people online trying to set up their GPUs for machine learning, it is often a nightmare, especially on unsupported GPUs. AMD GPUs notably don’t have any equivalent to Tensor Cores, and lack many of the plug-and-play optimizations Nvidia offers. One of the biggest pros of AMD GPUs is that they are often priced very competitively when compared to their Nvidia counterparts, however even with the better prices we don’t recommend buying any AMD GPUs simply because their machine learning ecosystem is simply much less developed than Nvidia’s.

## **Google TPUs**
While most developers accelerate their workloads with GPUs, Google has in more recent years released its AI ASIC, the Cloud TPU, or Tensor Processing Unit. TPUs are matrix multiplication engines, each consisting of an array of 128 x 128 multiply/accumulators, as well as a Vector Processing Unit used for general computation and a Scalar Processing Unit for managing dataflow and memory. TPUs come on boards with 4 chips, each with two cores, thus all TPU instances are in multiples of 8 cores. Currently, only the V2, V3, and V4 TPUs are available to use in Google Cloud. Google sells the usage of pods of up to 2048 cores of TPUs. Google now offers TPUs as either a Pod or a VM. With TPU pods you are given a VM that communicates with a TPU over gRPC, but with a VM instance, you are SSH into a VM connected to the TPU, thus giving you more control.

![](ReportImages/TPUlayout.png)
V3 TPU Layout

### **Usage**
Google’s TPUs are definitely not as plug-and-play as they would claim, and we think it’s worth mentioning some of the problems you may run into, as there is a lack of good resources when things go wrong. Getting a TPU to train our model was painful, as many of TensorFlow’s experimental features won’t work on TPUs (I tried four different ways of loading the data, that all worked on GPUs, but failed on TPUs), and setting it up required an entirely different data pipeline than what was originally used. Firstly, to use a TPU your data must be stored in a GCP Cloud Bucket, as this is the only way a cluster can read outside data (although I am not sure that this is the case with TPU VM instances), although this isn’t particularly hard to do. It’s worth noting that the TPUs have a 2GB protobuf limit, so you can’t just download files greater than 2GB. However, to use the entire dataset you can convert it to TFRecord files and upload those to your storage bucket. TPUs can only take TensorFlow datasets as training input, but these can be created by parsing TFRecord files. It's worth noting that there are experimental TensorFlow functions that allow you to keep the shape of your data when you save and load the TFRecord files, however, these don’t seem to work with TPUs, you have to use the standard TensorFlow functions to serialize your data, and reshape it after you load. For more efficient data loading you can prefetch your data and have it fed as a buffer into the TPU. Then in your code, you have to connect to the TPU before you do any training, and you have to set a cluster resolver and a distribution strategy. We used the automated ones, but you have the ability to use a custom strategy such as only using one core. Then when you compile and train your model it has to be within the strategy scope to actually use the TPU. When training TPUs, to get good performance you need to feed it large batch sizes, or the hardware isn’t really taken advantage of. For inference, there's not really anything different you have to do from running on a GPU or CPU. While we used Tensorflow for our code, Google now also supports Pytorch and Jax to run on TPUs.

### **Performance**

![](ReportImages/TPUtraining.png)

Here we compare the performance of a V2 TPU and an A100 and V100 all running out models in TensorFlow. Based on the highest-end GPUs, the TPU seems to underperform when it comes to training, especially considering the heavy price of the TPUs.      
        
![](ReportImages/TPUinference.png)

Here we also see the TPU underperform when compared to the A100 and V100, and the TPU also underperforms when compared to the higher-end GPU counterparts.

### **Pricing and Recommendation**
The on-demand pricing of TPUs depends on your region but is generally $4.50 per hour for a V2 and $8.00 for a V3. Making usage more expensive than an A100. For that reason, just based on training performance when compared to the higher-end Nvidia GPUs, we don’t recommend you train or deploy on TPUs, unless you have the ability to train your entire model on one of the free TPUs offered by Colab and Kaggle. Although performance may vary depending on your model, you seem to be much better off going with an A100 at a similar, but usually cheaper, price point. 

## **Apple Offerings**
Since the recent advent of ARM-based Apple Silicon Macs, Macbooks now have Apple GPUs rather than AMD Radeon Pro GPUs. Older intel Macs had discrete GPUs with separate memory for the CPU and GPU, however, with apple silicon, all the components are on a single SOC (system on chip) and thus the GPU and CPU access the same memory, and speeds up the communication between the CPU and GPU, as they don’t have to transfer data over PCIe. With Apple silicon, accelerators originally designed for mobile can run on a laptop or desktop, such as a Neural Engine (this is Apple’s NPU (Neural Processing Unit), which accelerates matrix multiplication and convolution), Machine Learning Accelerators (these are matrix multiplication accelerators), and hardware accelerated video encoding and decoding. As far as general processing goes, Apple chips have two kinds of cores, efficiency cores, and performance cores that allow for asymmetric multiprocessing where tasks are run on the different cores based on priority (performance vs power efficiency). 

![](ReportImages/m1ProLayout.png)
M1 Pro Layout

### **Tools, Frameworks, and APIs**
One of the main goals of Apple's APIs and Frameworks is that all the code and setup is the same for all devices. The Metal API can sort of be thought of as Apple's equivalent to CUDA or ROCm, as it is the API you interact with to use the GPU. There are now versions of Pytorch and Tensorflow that use Metal on the backend to allow you to train and do inference with the GPU. Following the Apple, docs to get this setup was very straightforward. CoreML is Apple’s machine learning Framework, which is how you deploy models that can take advantage of all of the accelerators on a device. CoreML has a Python package called coremltools that allows you to convert a Pytorch or Tensorflow model into CoreML. Converting is relatively simple, it is also possible to call the predict function on your model in Python. However, when converting our model, we tried essentially every possible combination of compute units (you can declare what you want your model to run on, although it defaults to .ALL, which runs on the CPU, GPU, and Neural Engine) input precision and compute precision, and were never able to get our model to use the Neural Engine or GPU. As seen below:

![](ReportImages/macBenchmark.png)

One convenient feature that Xcode has for ML models is a GUI that displays all the information about the model in a nice format, that also has a built-in benchmarking feature that shows what hardware was used on each layer, again as you can see here it only used the CPU:


While we’re not entirely sure why our model won’t use the hardware, we think it may be due to one or more layers not being supported by the GPU or Neural Engine and the CoreML just setting everything to the CPU.

![](ReportImages/m1benchmark.png)

### **Performance**
We benchmarked the 2020 M1 Macbook Pro with an 8-core GPU, and the 2021 M1 Pro Macbook Pro with a 16-core GPU.

![](ReportImages/appleTraining.png)

Here we compare the two Macbooks with an Nvidia T4 for reference, all training the same Tensorflow model. The M1 pro has close to the same training time as the T4, while the standard M1 is almost half as slow. The M1 pro has only half as many GPU cores, so this performance does seem to make sense intuitively.

![](ReportImages/appleInference.png)

When it comes to inference the rankings are similar with the M1 pro doing slightly better than the T4 in this case. We also see a similar effect where the 8 core GPU is about half as slow as the 16-core.

### **Pricing and Recommendation**
Everyone has heard of the notorious Apple tax, and this lineup is no different, although I would argue that these chips do offer much more value. However, it should be noted that 40 series laptops will be on the market in early 2023. Also since we didn’t compare to any other gaming/professional laptops with Nvidia GPUs, it is hard to make a recommendation. Generally, we wouldn’t expect someone to run a trading algorithm live on a laptop, so we think the training time should take priority when considering a laptop, and in this case, training time very much seems proportional to the number of GPU cores, thus it's probably best to select your laptop with that in mind. The current base model M1 Pro Macbook Pro starts at $2000, so the MBPs generally occupy the same price bracket as 3080 laptops, the highest-end Nvidia gaming laptop GPU. Also when selecting a laptop it's good to remember the amount of free and paid cloud options that will perform comparably. 

## **Frameworks We Use**d
### **Tensorflow**
Released by Google Brain in September 2015, Tensorflow represents the first initiative from big tech to create a machine learning framework. It combines multiple types of neural networks and algorithms on a Python/JavaScript frontend powered by an ultra-fast C++ backend. In May 2019, Google also launched Tensorflow Lite, a far less compute and memory-intensive version with a (usually) small accuracy tradeoff. This is used on iOS and other hardware with limited resources.

A key feature of Tensorflow is its high level of abstraction. It can be used at only a high level for quick experimentation of different models and ideas. You simply define a directed acyclic graph for the data to travel in, and the framework will take care of sending the data through each node. However, this means that the model structure can’t be changed dynamically, which can be a pain if the input length is not always the same. Even though Google has created Tensorflow Fold to solve this, it is not a part of the framework design. Debugging is also a chore since the only debugger you can use is tfdbg or print statements if that’s your type of thing.

Despite this, Tensorflow is still the most popular machine learning framework to date, currently used by many large tech companies such as Google, Airbnb, Twitter, Intel, Spotify, and more.
### **Pytorch**
Pytorch actually started as an internship project at Facebook by Adam Paszke. He created a Python version of Torch, an already existing deep-learning language written in Lua. However, it was later handed over to Facebook’s AI Research lab and they handled most of the development.

It addresses many of the issues with Tensorflow by being dynamic in its inherent design. You can create, modify, and execute nodes any time that you want. Since your code is manually executing and processing the values returned from each node, you’re able to use any normal debugging tools. It also gives you far more control since you can make any modifications to the data in between nodes, whereas in Tensorflow you can only use the modules they support. Pytorch offers an amazing balance of abstracting the math away from you but still giving you complete control over your model. 

Pytorch also has the strongest support for parallel computing. You can wrap your model in their DataParellel module to automatically utilize asynchronous execution and multiple device training. 

Facebook has turned up the heat on Google with PyTorch. Although Google has refuted it, there are rumors that they are slowly moving away from Tensorflow and working on a new deep-learning framework called JAX. PyTorch is used heavily by Microsoft in a wide range of their products and even by companies like Tesla for their autopilot system. Even though it’s only the second most popular machine learning framework, it is still far ahead of third and is still gaining traction at a much faster pace than Tensorflow. 

![](ReportImages/repoFramework.png)
![](ReportImages/paperFramework.png)

### **MXnet**
Although it’s far less popular than PyTorch or Tensorflow, MXnet is the last machine learning framework that has any popularity, as of today. It was created by the Apache Software Foundation in 2015, which is about the same time as PyTorch and Tensorflow. It supports a huge array of languages, ranging from Python, C++, Java, Julia, MATLAB, JavaScript, Go, R, Scala, Perl, and Wolfram Language.

When using the framework with the gluon API, it’s actually very similar to PyTorch. A lot of the syntax is very similar, and models in both frameworks are structured the same way. It’s not clear why MXnet doesn’t have much popularity due to its performance on paper being very similar to PyTorch as well. One reason could be that they don’t have a huge tech company backing their development, so new features and bug fixes often take much longer to implement. Also, their developer community is very small and their documentation is relatively poor compared to Tensorflow and PyTorch.

Even though it isn’t as popular as the big two, Amazon has chosen MXnet to be the machine learning framework of choice on AWS. It’s also used by many research universities, such as CMU, MIT, the University of Washington, and more. Even though it seems to be used by all these big institutions and companies, it was very difficult to get details about the framework.
Frameworks We Dropped

## **Frameworks We Dropped**
### **Caffee2**
This framework was first created by a PhD student at UC Berkeley and stands for “Convolutional Architecture for Fast Feature Embedding”. Facebook added many features to it and then relaunched it as Caffe2 in 2017. There isn’t too much about this framework because it was merged into PyTorch in just March 2018. At a high level, they kept most of PyTorch’s front end but added many backend features of Caffe2 to the new package.

### **Chainer**
Chainer doesn’t really offer any benefits over using PyTorch. Their front-ends are extremely similar, which is not a surprise since PyTorch actually started as a fork of Chainer with a different backend. With all the advancements Facebook has made with PyTorch, it doesn’t really make sense to use Chainer anymore unless there’s a specific use case that you need.

### **CNTK**
Microsoft Cognitive Toolkit, also known as CNTK, was created by Microsoft in 2016. A model is built using a directed graph structure, similar to Tensorflow. There aren’t many unique features on this framework since it was abandoned by Microsoft in 2019, and has not received any updates since.

### **MATLAB Machine Learning**
The machine learning modules were created by Mathworks, the company that manages MATLAB. There’s no inherent issue with this framework, but it uses MATLAB, which is a proprietary programming language that is barely used in the ML industry because it requires a subscription and the community is small with limited resources.

## **Framework Recommendations**

![](ReportImages/frameworkTraining.png)
![](ReportImages/frameworkInference.png)

There are really only three candidates for a new machine learning project to use; Tensorflow, Pytorch, and MXnet. Tensorflow had the highest training performance, but also the worst inference performance. Pytorch usually trailed a bit behind Tensorflow in training performance but had the best inference performance out of the three. MXnet was neither the best nor the worst in training nor inference.

Based on the data, we would recommend using Pytorch, since it has the most relevance today and the best overall performance out of the three. Looking at the graph, we see that Pytorch has comparable training performance to the other frameworks but maintains a significant inference performance lead. Inference performance would be much more important to software that is running on live data.

Tensorflow is another good option since it is very easy to use for beginners, but we can assume that most high-frequency trading developers are not beginner programmers, so it doesn't make as much sense here. As stated earlier, there are rumors of a new deep learning framework by Google, which means that Tensorflow might only have a few more years of new features. 

MXnet is not recommended as it lacks many modern features since it has such a small number of users and developers working on it. The newest version of MXnet doesn’t even support the newest versions of CUDA and NumPy.

## **Cloud GPU Providers, Deployment, and Local Deployment**
Training and running deep learning neural networks like what we do for this project often require hardware acceleration, and the most popular hardware accelerator is the graphics processing unit or GPU. In our project, we explored different cloud services for the usage of GPU hardware acceleration. Now we present to you our findings.

We compared the GPU cloud offerings of the large public clouds to independent alternatives, startups, and everything in between. As a result, we have a good understanding of GPU cloud options available today – especially for machine learning and deep learning applications.

GPU cloud providers often use different units of measurement with different sensible defaults. GPU machine specs can vary wildly from cloud to cloud – with different instances or machine sub-groupings and different pricing conventions. When you are evaluating which providers to use, we would suggest standardizing all the offerings to a single pricing method: price per GPU per unit of time. We did not include preemptible or interruptible GPU providers in our project.

In terms of cloud computing, there are three major players in the field: AWS, Azure, and GCP, which we have focused on for our project. However, we do want to point out that there are many viable alternatives to the GPU cloud instances offered by big tech companies, for example, PaperSpace, Gradient, Kaggle, and FloydHub. Major differences we noticed:
1. Range of Services: Big tech companies like GCP and AWS offer a wide range of cloud services beyond GPU services, including storage, computing, databases, and AI tools. They can also provide services on a massive scale. On the other hand, smaller companies often specialize in only GPU services and offer a more wide range of selections of GPUs. solutions. Hence if you are looking for something more customized and specialized, it might be helpful to look into smaller companies.
2. Cost: Cost is often more complicated when using big tech services.
    * The cost of time to set it up is often more for big companies. There are a lot more options to choose from and consider
    when setting up instances on big platforms. Also, during our usage of GCP, we found that there could be time when it became unreliable and unpredictable because the location you selected may ran out of the GPUs you want, which increases the cost of time.
    * Generally speaking, big cloud providers can afford to offer GPU services at a lower cost compared to smaller companies. However, things could add up when the service you chose also includes resources you don’t need, whereas smaller companies can offer more flexible and customized pricing options which may cost less at the end. 
    * Things that could affect pricing to consider: storage, network performance, and data ingress/egress.
3. Service Level Agreements (SLAs): Large companies have robust SLAs, ensuring a high level of uptime and reliability for their GPU services, whereas smaller companies generally have less guarantee for SLAs. Hence we would suggest going with big companies if you value uptime and reliability.

For your information, we include a complete comparison chart borrowed from https://cloud-gpus.com/.

![](ReportImages/cloudOfferings.png)

As you can see from the chart, GCP offers the most free credit for first-time users and has the widest range of options for different GPUs, which is what we want for this project. Hence we chose GCP for our project. Now we will dive into some interesting things we found while using GCP and some tips we have.
1. You can get $300 of free credit to use for Google Cloud Platform products when you sign up for a GCP account.
2. If you would like to use GPU services on GCP. You will have to upgrade to a paid account by inputting your card information. But don’t worry, you won’t be charged unless you ran out of those $300 credits. 
3. You will have to request for GPU access on GCP when it’s your first time. It should be approved very fast. We put “For Machine Learning” under the Reason area. Also, you will need to request again if you are trying to use A100 due to its limited availability.
4. Make sure to select the VM with pre-installed libraries and frameworks if you know which framework you will be using. 
5. During busy hours, you might have to retry creating instances with GPU multiple times because the one you want might not be available in the region you selected. Make sure to read the resource location tab and find out which area has the most GPUs you want.
6. You can use Google Colab and Kaggle with GCP resources for ease of use. There are several advantages to using Colab and Kaggle over pure Python, especially for those who are new to the field or working as a team, for example, a more user-friendly interface, built-in tools, libraries, etc. Here is how to connect GPU to Colab: https://research.google.com/colaboratory/marketplace.html. And here is how to connect the GPU to Kaggle: https://www.kaggle.com/code/alexisbcook/get-started-with-google-cloud-platform/notebook. 
7. Some great smaller cloud GPU providers we found: Vultr, PaperSpace, Lambda Labs, and Jarvis Labs.

## **Local**
We also trained locally with our own GPUs. The process is relatively simple by just running the python script in the shell, whether it’s Windows or Mac/Linux. Dependency would be the only thing you need to worry about when running locally. A good way to make sure everything works correctly is to include all of the dependencies in a text file such as “requirements.txt” and run pip install with it.

While implementing Apple Core ML, we encountered various issues and it currently does not support our model when we converted it from Tensorflow hence we were not able to use Apple Neural Engine.

## **Cloud vs Local**
Now we present our comparison between Cloud GPUs and In-house GPUs:

Why you want to use cloud GPUs:
Accessibility: access powerful GPUs with an internet connection from anywhere. 
Scalability: allows you to scale your computing resources up or down as needed, which is useful if you have periods of high and low demand. This can save you time and money compared to having to purchase and maintain a large number of local GPUs.
Cost-effectiveness: Using cloud GPU providers can be more cost-effective than purchasing and maintaining a large number of local GPUs, especially for small businesses or individuals who do not have the resources to invest in expensive hardware.
Ease of use: Cloud GPU providers typically have user-friendly interfaces and provide easy-to-use tools for deploying and managing GPU instances.

Why you want to host local GPUs:
Data Security: It can create a dependency on the cloud service provider, as they have control over the data and the infrastructure that hosts it. This dependency can make it difficult or expensive to move the data to a different provider or to bring it back on-premises if needed.
Bandwidth: Depending on the quality of your internet connection, the transfer of data to and from cloud GPU providers can result in slowdowns. This can impact the performance of resource-intensive applications.
Dependency on internet connection: If your internet connection goes down, you won't be able to access your cloud GPUs, which can be a problem if you're working on a time-sensitive project.
Cost: Although cloud GPU providers can be more cost-effective than purchasing local GPUs, the cost of using cloud GPUs can add up quickly, especially if you're using them for extended periods of time.

In terms of Cloud GPU Providers, Deployment, and Local Deployment, for students or beginners like us, we would recommend using big cloud providers since it offers the most free credits and you would have access to a wide range of services. For small professional organizations, we suggest reaching out to smaller cloud GPU providers and they could provide customized solutions that fit your needs perfectly. However, we do suggest transiting to on-premise GPUs if you have enough capital. For large organizations, we recommend using on-premise GPUs since they usually have more capital resources.

## **Conclusion**
In conclusion, our project focused on the application of machine learning in algorithmic trading, with a specific emphasis on comparing the performance of various hardware accelerators and frameworks across different cloud platforms. Through our research, we have provided insights into the benefits and limitations of the LSTM deep learning model we used, as well as the performance, analysis, and recommendations of different hardware accelerators. We have also analyzed popular frameworks we used such as Tensorflow, Pytorch, and MXnet, in addition to evaluating the advantages and disadvantages of using cloud GPU providers versus hosting local GPUs. We found that the choice of hardware, framework, and deployment option can significantly impact the performance and cost-effectiveness of a machine learning project. Overall, our project demonstrated the potential of deep learning for algorithmic trading and provided insights into the optimal configurations for different hardware components, frameworks, and deployment strategies. We hope our findings will serve as a helpful resource for anyone seeking to delve deeper into the fascinating and constantly advancing realm of machine learning and its applications in the field of algorithmic trading.

## **Postmortem Summary:**
### **John Hofmann**
1. **What did you specifically do individually for this project?**  
I wrote the Chainer and CNTK code we dropped. I converted the model to Apple CoreML and benchmarked it. I benchmarked Tensorflow on the cloud GPUs and TPU. I created the second data pipeline for GCP and TPUs, and setup everything for using GCP and GCP buckets. I converted the Tensorflow code to work on TPUs (major headache, required a second data pipeline and major changes to the way data was fed to the model for training), and converted the Tensorflow code to work with Mixed-Precision. I did most of the Hyper Parameter Tuning (not hard, just a time suck), and wrote the hardware portion of the report.
2. **What did you learn as a result of doing your project?**        
Machine learning was pretty new to me, so I learned quite a bit about the tools as well as formatting data to input to models and the tuning of said models. I had also never really done anything cloud related, and learning to use GCP and its API were useful to learn.
3. **If you had a time machine and could go back to the beginning, what would you have done differently?**      
I would have focused much less on the range of frameworks, since we ended up dropping most of them anyways, and focusing more on the different hardware, or possibly looked into other kinds of models, although ours did use a wide variety of layer types.
4. **If you were to continue working on this project, what would you continue to do to improve it, how, and why?**      
I think the biggest thing we didn't get to test enough was gaming GPUs. If I were to continue the project I would like to run more comprehensive benchmarks on the current 40 series Nvidia GPUs, as well as the 30 series. The value of gaming GPUs at least on paper seem much better that most of the Professional cards that Nvidia sells.

5. **What advice do you offer to future students taking this course and working on their semester long project (be sides “start earlier”... everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.**     
I think you have to set hard deadlines and do your best to hold everyone to those deadlines. School makes things hard, but you have to make sure everyone is making consistant progress throughout the semester or you won't finish on time. Also if you have a large ammount of data for your project, try and find either a cloud sorage solution (you get unlimited Drive storage with your school account) or have an external hard drive that everyone can burn onto their machines. Also look at the commit history of repos for things you are thinking about using, as it is a good way to check if a project is dead or not.

### **Rahul Gupta**
1. **What did you specifically do individually for this project?**  
I wrote the caffe framework code, which was eventually dropped, and I also wrote the MXnet framework code, which was one of the three main frameworks that our project focused on. I also set up all the environments for our benchmark to run in, such as the vagrant setup, package installations (which was surprisingly a headache with three different frameworks running at the same time), and the scripts to run each component of our project. I also wrote the benchmarking file that parses the different options and displays the results for each framework. Finally, I wrote the frameworks part of the final report.
2. **What did you learn as a result of doing your project?**        
I knew the basics of machine learning, but I had never designed a model using one of the popular frameworks before. I learned that these frameworks abstract so much of the complex math away from the user, so it's not very difficult to create a model if you know the layers already.
3. **If you had a time machine and could go back to the beginning, what would you have done differently?**      
I agree with John and would have focused more on the different types of hardware, as that was the focus of our project. There are anyways only three relevant frameworks today, so it makes sense to focus more on their different optimaztions rather than making unoptimized versions of more frameworks.
4. **If you were to continue working on this project, what would you continue to do to improve it, how, and why?**      
I would continue to experiment with more types of hardware. For example, we couldn't get CoreML to work, and I think the results would have been very interesting since Apple silicon has never been common for machine learning use. I also think we could have expiremented with Tensorflow Lite, which is a version that sacrifices a bit of accuracy for much less compute power since it's designed to be run on mobile devices.  
5. **What advice do you offer to future students taking this course and working on their semester long project (be sides “start earlier”... everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.**     
I would be very realistic about the goals of the project before you start. Once you factor all the other things that you have to do during the semester, it's likely that you won't be able to accomplish as much as you think at first. It's also easy to fall behind as there's no set time or assignments when working on a semester-long project, so I would set aside time each week to work on the project, in the same way that you would attend lecture for a class.

### **Ruidi Huang**
1. **What did you specifically do individually for this project?**   
I wrote the code in both Matlab and PaddlePaddle frameworks which we dropped later, and implemented the model in Pytorch - one of the three primary frameworks we analyzed. I also benchmarked the Pytorch model with different cloud GPUs on GCP, and conducted research on cloud providers which informed my writing for the Cloud GPU Providers, Deployment, and Local Deployment section of our report.

2. **What did you learn as a result of doing your project?**    
I learned about the benefits and limitations of different hardware accelerators, frameworks, and cloud platforms for deep learning in algorithmic trading. I also gained knowledge in using various tools and services for data preprocessing, model training, and deployment. Furthermore, I developed skills in collaborating as a team and presenting technical information in a clear and concise manner.

3. **If you had a time machine and could go back to the beginning, what would you have done differently?**  
If I could go back to the beginning, I would have invested more time and resources into experimenting with a wider range of hardware accelerators and cloud resources. I would have conducted more thorough technical analyses on different hardware and frameworks, and created more comprehensive visualizations to make our findings more accessible and understandable. Additionally, I would have spent more time researching the industry standards for algorithmic trading in each top firm (if applicable) to better understand how our project's model and results compare to current practices.

4. **If you were to continue working on this project, what would you continue to do to improve it, how, and why?**   
I would focus on expanding the scope of the project to explore hardware and related topics that have not been extensively covered before. I would investigate the integration of reinforcement learning techniques into algorithmic trading models. I would also like to expand our analysis to include more cloud platforms and independent providers, as well as exploring more in-depth technical analysis and visualizations of the results. Moreover, I would aim to integrate the model with a trading platform to evaluate its performance in a real-world scenario. 
  
5. **What advice do you offer to future students taking this course and working on their semester long project (be sides “start earlier”... everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.**    
Start the project by clearly defining the problem statement and the objectives of the project. This will help in selecting appropriate data, models, and tools. It's important to stay organized, keep track of deadlines, and communicate effectively with your team. Additionally, be open to exploring new ideas and technologies, and don't be afraid to ask for help or guidance when needed. Lastly, make sure to document your progress and learn from your mistakes, as it will help you improve in future projects. 

### **Yun (Tiger) Wang**
1. **What did you specifically do individually for this project?**   
I reviewed multiple papers; selected and implemented the LSTM + CNN model in Tensorflow in Google Colab. I wrote the script for data parsing and preparation. I'm responsible for introducing the model in the final report.

2. **What did you learn as a result of doing your project?**    
Creativity is the limit. Combining CNN and LSTM gives you ≥50% accuracy. I have to continuously learn from the best research.

3. **If you had a time machine and could go back to the beginning, what would you have done differently?**  
I would love to explore more papers and potentially use Transformers for this project. With recent breakthrough in Natural Langauge Processing, the similar knowledge should be transferable to the stock market (or time series analysis).

4. **If you were to continue working on this project, what would you continue to do to improve it, how, and why?**   
I would definitely use Transformers. Transformers combining with other models could be the next paradigm for any machine Learning project in the near future and a comprehensive hardware analysis would be useful.

5. **What advice do you offer to future students taking this course and working on their semester long project (be sides “start earlier”... everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.**    
Clear communication, resonable expectation, and candid feedback are the key. It won't hurt to have a concrete weekly plan that considers exams. The plan should also give grace to teammates; afterall, it is a relatively low-stake school project. Moreover, passion is also important, as it is a semester-long project.

## Refrences
https://www.nvidia.com/en-us/data-center/tensor-cores/
https://www.nvidia.com/en-us/geforce/graphics-cards/40-series/
https://blog.paperspace.com/understanding-tensor-cores/#:~:text=Tensor%20Cores%20are%20specialized%20cores,4%20FP16%20or%20FP32%20matrix.
https://developer.nvidia.com/tensorrt 
https://fullstackdeeplearning.com/cloud-gpus/
https://cloud.google.com/tpu/docs/system-architecture-tpu-vm
https://console.cloud.google.com/compute/tpus 
https://www.anandtech.com/show/17019/apple-announced-m1-pro-m1-max-giant-new-socs-with-allout-performance 
https://developer.apple.com/videos/play/wwdc2020/10686/#:~:text=Machines%20with%20a%20discrete%20GPU,working%20over%20the%20same%20memory. 
https://nod.ai/comparing-apple-m1-with-amx2-m1-with-neon/
https://www.infoworld.com/article/3278008/what-is-tensorflow-the-machine-learning-library-explained.html
https://www.simplilearn.com/keras-vs-tensorflow-vs-pytorch-article
https://hub.packtpub.com/tensorflow-always-tops-machine-learning-artificial-intelligence-tool-surveys/
https://hub.packtpub.com/tensorflow-always-tops-machine-learning-artificial-intelligence-tool-surveys/
https://medium.com/@alec.mccabe93/history-and-basics-of-tensorflow-eaee87c6aef0
https://medium.com/@alec.mccabe93/history-and-basics-of-tensorflow-eaee87c6aef0
https://www.assemblyai.com/blog/pytorch-vs-tensorflow-in-2023/
