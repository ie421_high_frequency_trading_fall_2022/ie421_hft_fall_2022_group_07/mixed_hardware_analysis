# preprocess.py
# # Install & Import Libraries
# 
# 


# install requirements



import numpy as np
from keras.utils import np_utils

import pandas as pd
import json
from collections import deque

import os
import sys
import time
# import threading

#drive.mount('/content/drive', force_remount=True)
# drive.mount("/ShardDrives/", force_remount=True)


# # Utils


# file_path = file_name
def writeJsonFile(file_path, json_string):
    # write to a json file
    # Directly from dictionary
    
    with open(file_path, 'w') as outfile:
        json.dump(json_string, outfile)

# read;
def readJsonFile(file_path):
    # read form a json file
    
    with open(file_path) as json_file:
        data = json.load(json_file)
        return data

# increasing ordered by name
def getAllFileNames(folder_path) -> list:
    
    data_filenames : object  = os.scandir(folder_path)
    data_filename_list : list = []
    for entry in data_filenames:
        if (entry.is_file()):
            data_filename_list.append(entry.name)
    data_filename_list.sort()
    return data_filename_list

def isEmptyDir(path) -> bool:
    # Check if the path is a directory
    if not os.path.isdir(path):
        raise ValueError("{} is not a directory".format(path))

    # Check if the directory is empty
    return len(os.listdir(path)) == 0

import os

def deleteAllInside(path):
    # Check if the path is a directory
    if not os.path.isdir(path):
        raise ValueError("{} is not a directory".format(path))

    # Iterate over the items in the directory
    for item in os.listdir(path):
        # Construct the full path to the item
        item_path = os.path.join(path, item)

        # Check if the item is a directory
        if os.path.isdir(item_path):
            # If it is a directory, delete all items inside it recursively
            deleteAllInside(item_path)

            # Delete the empty directory
            os.rmdir(item_path)
        else:
            # If it is a file, delete it
            os.remove(item_path)


def prevDay(cur_time : str, n : int):
    from datetime import datetime
    cur_datetime = datetime.strptime(cur_time[:10], ("%Y-%m-%d"))
    return_datetime = cur_datetime - datetime.timedelta(days=n)
    return (return_datetime.strftime("%Y-%m-%d"))



# # Constants
# 
# Please past paths as indicated


# paste your IEX_AUG01_2021-2022 folder path
raw_data_folder_path : str = '/vagrant/data/book_snapshots'
# paste your IEX_DATA folder path
normalized_data_folder_path : str = '/vagrant/data/intermediate_data'
# paste your desired model's data and labels folder path
model_data_label_folder_path : str = '/vagrant/data/intermediate_data'

# this will be the float type for input data, labels, and weights...
# a type alias
fp_type = np.float32

# how many data are we selecting from each normalized data file
# e.g. 75000 means from each normalized data file, we choose 75000 amount of (100, 40)
each_file_data_num : int = 75000
file_num : int = 4
total_data_num : int = file_num * each_file_data_num  # 300000 / 4 = 75000

# hyper parameters
look_back : int = 100
label_threshold : fp_type = 0.000025 # called as alpha in the paper

# start and end date for data processing
start_date : str = '20210802'
end_date : str = '20220901'


# # Data Processing


# ### Normalization


# # #   According to the paper, 
#
# #     X:
#           X = [x1,x2,··· ,xt,··· ,x100]T ∈ R^(100×40); 
#           xt = [pa_i(t), va_i(t), pb_i(t), vb_i(t)] i from 1 to 10. 
#           pa_i(t) := the i_th bestask price at t; va_i := the volume for pa_i(t)
#           xt is normalised by z-score of previous 5 days of data (i.e. mean and std of prev 5 days)
#           Do not consider current day when calculating z-score (kinda makes sensmar: ket is not that continuous)
#           
# #     Y/lt:
#           lt = (m+(t) − m−(t)) / m-(t)
#           m+(t) := 1/k * (sum of previous k mid-prices and current mid-price)   
#           m-(t) := 1/k * (sum of next k mid-prices)  
#
# #     Note:
#           For the ease of feature extraction / data labelling, 
#           We only use data from 20220809 to 20220901 and time 14:00 to 19:30 
#           for training, evaluating, and testing inputs. 
#           Spatiality is crucial in CNNs

COMBINED_COLUMN_NAMES = ['COLLECTION_TIME', 'LABELS',
                         'BID_PRICE_1', 'BID_SIZE_1', 'ASK_PRICE_1', 'ASK_SIZE_1',
                         'BID_PRICE_2', 'BID_SIZE_2', 'ASK_PRICE_2', 'ASK_SIZE_2',
                         'BID_PRICE_3', 'BID_SIZE_3', 'ASK_PRICE_3', 'ASK_SIZE_3',
                         'BID_PRICE_4', 'BID_SIZE_4', 'ASK_PRICE_4', 'ASK_SIZE_4',
                         'BID_PRICE_5', 'BID_SIZE_5', 'ASK_PRICE_5', 'ASK_SIZE_5',
                         'BID_PRICE_6', 'BID_SIZE_6', 'ASK_PRICE_6', 'ASK_SIZE_6',
                         'BID_PRICE_7', 'BID_SIZE_7', 'ASK_PRICE_7', 'ASK_SIZE_7',
                         'BID_PRICE_8', 'BID_SIZE_8', 'ASK_PRICE_8', 'ASK_SIZE_8',
                         'BID_PRICE_9', 'BID_SIZE_9', 'ASK_PRICE_9', 'ASK_SIZE_9',
                         'BID_PRICE_10', 'BID_SIZE_10', 'ASK_PRICE_10', 'ASK_SIZE_10']

DATA_COLUMN_NAMES = ['BID_PRICE_1', 'BID_SIZE_1', 'ASK_PRICE_1', 'ASK_SIZE_1',
                     'BID_PRICE_2', 'BID_SIZE_2', 'ASK_PRICE_2', 'ASK_SIZE_2',
                     'BID_PRICE_3', 'BID_SIZE_3', 'ASK_PRICE_3', 'ASK_SIZE_3',
                     'BID_PRICE_4', 'BID_SIZE_4', 'ASK_PRICE_4', 'ASK_SIZE_4',
                     'BID_PRICE_5', 'BID_SIZE_5', 'ASK_PRICE_5', 'ASK_SIZE_5',
                     'BID_PRICE_6', 'BID_SIZE_6', 'ASK_PRICE_6', 'ASK_SIZE_6',
                     'BID_PRICE_7', 'BID_SIZE_7', 'ASK_PRICE_7', 'ASK_SIZE_7',
                     'BID_PRICE_8', 'BID_SIZE_8', 'ASK_PRICE_8', 'ASK_SIZE_8',
                     'BID_PRICE_9', 'BID_SIZE_9', 'ASK_PRICE_9', 'ASK_SIZE_9',
                     'BID_PRICE_10', 'BID_SIZE_10', 'ASK_PRICE_10', 'ASK_SIZE_10']

csv_columns : str = 'COLLECTION_TIME,LABELS,BID_PRICE_1,BID_SIZE_1,ASK_PRICE_1,ASK_SIZE_1,BID_PRICE_2,BID_SIZE_2,ASK_PRICE_2,ASK_SIZE_2,BID_PRICE_3,BID_SIZE_3,ASK_PRICE_3,ASK_SIZE_3,BID_PRICE_4,BID_SIZE_4,ASK_PRICE_4,ASK_SIZE_4,BID_PRICE_5,BID_SIZE_5,ASK_PRICE_5,ASK_SIZE_5,BID_PRICE_6,BID_SIZE_6,ASK_PRICE_6,ASK_SIZE_6,BID_PRICE_7,BID_SIZE_7,ASK_PRICE_7,ASK_SIZE_7,BID_PRICE_8,BID_SIZE_8,ASK_PRICE_8,ASK_SIZE_8,BID_PRICE_9,BID_SIZE_9,ASK_PRICE_9,ASK_SIZE_9,BID_PRICE_10,BID_SIZE_10,ASK_PRICE_10,ASK_SIZE_10'

# For X and Y, we only use time from 14:00 to 19:30
# For std, previous 5 days (may think of a more dynamic std)
# Save to Drive as csv -> download it -> convert to a parquest hdfs -> upload it to Drive
# skip_5 := skip writing first 5 files or not
def preprocess(data_folder : str = raw_data_folder_path, 
               dest_folder : str = normalized_data_folder_path,
               mode : str = 'w',
               prediction_horizon : int = look_back,
               label_threshold : fp_type = label_threshold,
               start_date : str = start_date,
               end_date : str = end_date,
               skip_5 : bool = True,
               fp_type = fp_type):
    print("\n\nIn preprocess:")

    # read -> drop labels -> restrict time -> add LABELS -> reindex
    def readCleanData(filename : str, only_data : bool = False):
        
        # print('reading file: ', filename, ' ...')
        cur_df : pd.DataFrame = pd.read_csv(f'{data_folder}/{filename}')
        date : str = f'{filename[:4]}-{filename[4:6]}-{filename[6:8]}' # get the year, month, and date from the filename
        # restrict to 14:00 to 19:30
        cur_df = cur_df.loc[cur_df['COLLECTION_TIME'] >= f'{date} 14:00']
        cur_df = cur_df.loc[cur_df['COLLECTION_TIME'] <= f'{date} 19:30']
        if not only_data: # also want LABELS and COLLECTION_TIME
            cur_df = cur_df.drop(labels=['MESSAGE_ID', 'MESSAGE_TYPE', 'SYMBOL'], axis=1)
            length = len(cur_df['ASK_SIZE_10'])
            cur_df['LABELS'] = [-2] * length # '' := unlabelled
            cur_df = cur_df.reindex(columns=COMBINED_COLUMN_NAMES)
            return cur_df, length
        elif only_data: # we only want the 40 features (columns)
            cur_df = cur_df.drop(labels=['COLLECTION_TIME', 'MESSAGE_ID', 'MESSAGE_TYPE', 'SYMBOL'], axis=1)
            cur_df = cur_df.reindex(columns=DATA_COLUMN_NAMES) # no "LABELS"
            return cur_df    


    
    filenames : list = getAllFileNames(data_folder)
    filenames.sort() # sort the filenames in acsending order
    filenames = [item for item in filenames if ((item >= start_date) and (item <= end_date))]
    print(f'\t{len(filenames)} raw data files: {filenames}')
    dest_path : str = f'{dest_folder}/{filenames[0][:8]}-{filenames[len(filenames)-1][:8]}_{prediction_horizon}_{label_threshold}.csv'

    # write index columns to outfile
    if mode == 'w':
        with open(dest_path, 'w') as outfile:
            outfile.write(',' + csv_columns + '\n')
            outfile.close()
    elif mode == 'a':
        with open(dest_path, 'a') as outfile:
            outfile.write(',' + csv_columns + '\n')
            outfile.close()

    # parsing
    processed_file_count : int = 5
    outfile = open(dest_path, 'a')
    print(f'\toutfile openned at {dest_path}')
    total_file_count = len(filenames)
    def writeToOutfile(df : pd.DataFrame, length : int, proc_file_count : int = processed_file_count, retries : int = 3):
        if retries == 0:
            return proc_file_count
        try:
            df_csv_str : str = ((df.to_csv(index=False)).split('\n', 1))[1] # only want the data; not the labels
            outfile.write(df_csv_str)
            proc_file_count += 1
            print(f'\t\t{proc_file_count}: {filename} normalised and processed (labels); written, length: {length}\n')
            return proc_file_count
        except OSError:
            time.sleep(1)
            print("encountered OSError, retrying...")
            return writeToOutfile(df, length, proc_file_count, retries - 1)

    if skip_5 == False:
        # first 5, just copy them in
        for i in range(min(total_file_count, 5)):
            filename : str = filenames[i]
            
            cur_df, length = readCleanData(filename)
            writeToOutfile(cur_df, length)
    
    # after 5, add labels and normalise
    print('\tfirst 5 files processed\n\n\tstart labeling and normalise:')
    down : int = 0
    same : int = 0
    up : int = 0
    for i in range(5, total_file_count):
        filename : str = filenames[i]
        cur_df, length = readCleanData(filename)
        
        # concat the cur_df with previous and future (size of prediction horizon)
        def addLabels(k = prediction_horizon, alpha = label_threshold):
            # print(f'\t\tlabelling {filename}; alpha: {alpha}')
            mid_prices : np.ndarray = ((cur_df['BID_PRICE_1'] + cur_df['ASK_PRICE_1']) / 2).to_numpy().astype(fp_type)
            labels : np.ndarray = cur_df['LABELS'].to_numpy().astype(fp_type)
            for j in range(k, (len(cur_df) - k)):
                weight : fp_type = 1 / k
                m_prev : fp_type = weight * ((mid_prices[j-k+1 : j+1]).sum()) # include the current; total 100
                m_futr : fp_type = weight * ((mid_prices[j+1 : j+1+k]).sum()) # exclude the current; total 100
                prctg_chg : fp_type = (m_futr - m_prev) / m_prev
                if prctg_chg >= alpha:
                    labels[j] = 1
                elif prctg_chg <= (-alpha):
                    labels[j] = -1
                else:
                    labels[j] = 0
            cur_df['LABELS'] = labels

        # normalise
        # We use previous 5 days' mean & std to normalise
        def normalise():
            # print(f'\t\tnormalising {filename}')
            prev_5_day_filenames : list = [filenames[j] for j in range(i-5, i)] # i is at least 5
            prev_5_sum : dict = {}
            # for each prev 5 files, for each data index, add/append/concatenate the data together
            for prev_filename in prev_5_day_filenames:
                prev_df = readCleanData(prev_filename, True) # no labels; time restricted
                for datum_name in DATA_COLUMN_NAMES: 
                    # append all prev 5 day data together
                    col_data = (prev_df[datum_name].to_numpy()).astype(fp_type)
                    try:
                        prev_5_sum[datum_name] = np.concatenate((prev_5_sum[datum_name], col_data), axis=0)
                    except KeyError:
                        prev_5_sum[datum_name] = col_data
            for key, value in prev_5_sum.items():
                cur_df[key] = (cur_df[key] - value.mean()) / value.std()

        addLabels()
        normalise()
        cur_count : tuple = (len(cur_df.loc[cur_df['LABELS'] == -1]), len(cur_df.loc[cur_df['LABELS'] == 0]), len(cur_df.loc[cur_df['LABELS'] == 1]))
        down += cur_count[0]
        same += cur_count[1]
        up += cur_count[2]
        print(f'\t\t{filename} has (down, same, up) := {cur_count}')
        processed_file_count = writeToOutfile(cur_df, length, processed_file_count)

    new_path : str = f'{dest_path[:-4]}_({down},{same},{up}).csv'
    outfile.close()
    os.rename(dest_path, new_path)
    print(f'\trename to {new_path}')



# ### Make Input & Labels


cols = "COLLECTION_TIME,LABELS,BID_PRICE_1,BID_SIZE_1,ASK_PRICE_1,ASK_SIZE_1,BID_PRICE_2,BID_SIZE_2,ASK_PRICE_2,ASK_SIZE_2,BID_PRICE_3,BID_SIZE_3,ASK_PRICE_3,ASK_SIZE_3,BID_PRICE_4,BID_SIZE_4,ASK_PRICE_4,ASK_SIZE_4,BID_PRICE_5,BID_SIZE_5,ASK_PRICE_5,ASK_SIZE_5,BID_PRICE_6,BID_SIZE_6,ASK_PRICE_6,ASK_SIZE_6,BID_PRICE_7,BID_SIZE_7,ASK_PRICE_7,ASK_SIZE_7,BID_PRICE_8,BID_SIZE_8,ASK_PRICE_8,ASK_SIZE_8,BID_PRICE_9,BID_SIZE_9,ASK_PRICE_9,ASK_SIZE_9,BID_PRICE_10,BID_SIZE_10,ASK_PRICE_10,ASK_SIZE_10,EXTRA".split(',')

import random

def getPickedRowsIdx(total_rows : int, each_file_data_num : int) -> deque:
    # print("\t\ttype total row: ", type(total_rows))
    if total_rows <= 100: return []

    
    rows = range(100, total_rows) # [100, ..., total_rows-1]

    # Use the random.sample function to choose each_file_data_num numbers from the list
    # without replacement
    random_numbers : list = random.sample(rows, each_file_data_num)
    random_numbers.sort()
    return deque(random_numbers)


# ##### Regular (Rahul)


# only one file
def regularParse(filepath : str):
    print("\nIn regularParse (Rahul):")
    df = pd.read_csv(filepath)
    df.columns = cols
    df.drop('EXTRA', axis=1, inplace=True)

    picked_rows = getPickedRowsIdx(10 ** 5)

    prev_rows = 100
    training_data_x = np.zeros((len(picked_rows), prev_rows, df.shape[1] - 2))
    training_data_y = np.zeros((len(picked_rows), 1))
    for i, index in enumerate(picked_rows):
        training_data_x[i] = df.iloc[index-prev_rows:index, 2:]
        training_data_y[i] = df.iloc[index, 1]

    training_data_x = training_data_x.reshape(training_data_x.shape + (1,))
    training_data_y = np_utils.to_categorical(training_data_y, 3)

    with open('train.npy', 'wb') as f:
        np.save(f, training_data_x, allow_pickle=True)
        np.save(f, training_data_y, allow_pickle=True)


# ##### Low-Memory (Tiger)


def lowMemParse(data_folder : str = normalized_data_folder_path, # normalised data folder
                dest_folder : str = model_data_label_folder_path,
                each_file_rows : int = each_file_data_num,
                look_back : int = look_back,
                fp_type = fp_type,
                input_out : str = f'data_{(total_data_num, look_back)}.npy', 
                labels_out : str = f'labels_{(total_data_num, look_back)}.npy'):
    print("\n\nIn lowMemParse (Tiger):")

    

    # open output files
    input_data_outfile = open(f'{dest_folder}/{input_out}', 'wb')
    labels_outfile = open(f'{dest_folder}/{labels_out}', 'wb')
    print(f'\toutput files opened at folder {dest_folder}')

    # get normalised data filenames
    data_filenames = getAllFileNames(data_folder)

    # # e.g. if lookback is 100, we want previous 99 and current 1.
    # # so, data_buffer_arr, which contains previous data, should only has length of 99.
    # data_buffer_arr_len = look_back - 1

    # iterate through all files
    print(f'\titerate through all files')
    for filename in data_filenames:
        input_data_buffer_arr = deque()
        input_data_np = np.empty((each_file_rows, look_back, 40))
        labels_np = np.empty((each_file_rows, 1))
        line_count = 0
        picked_count = 0

        label_count : list = (filename.split('('))[1].split(')')
        label_count : list = np.array(label_count[0].split(',')).astype(fp_type)
        print(f'\t\t{filename}, total label count: {int(label_count.sum())}\n\t\trandomly sample rows to be the model input data...')
        picked_rows = getPickedRowsIdx(int(label_count.sum()), each_file_rows)
        while len(picked_rows) >= 1:
            for line in open(f'{data_folder}/{filename}', 'r'):
                cur_row = line_count
                line_count += 1
                if cur_row == 0: # the first line
                    continue
                
                split_arr = line.split(',')
                data = np.array((split_arr[2:])).astype(fp_type)
                label = np.array(split_arr[1]).astype(fp_type)

                # update input_data_buffer_arr
                if len(input_data_buffer_arr) < look_back: # add from cur_row 1 to 99
                    input_data_buffer_arr.append(data)
                elif len(input_data_buffer_arr) == look_back:
                    input_data_buffer_arr.popleft()
                    input_data_buffer_arr.append(data)
                else:
                    assert(len(input_data_buffer_arr) <= 100) # 99 previous + 1 current

                # select rows and its look_back if it is picked
                if picked_rows[0] == (cur_row): # it is the picked row
                    # print(picked_count, len(picked_rows), picked_rows[0], len(input_data_buffer_arr))
                    if (picked_count % (each_file_rows/5)) == 0: print(f'\t\t\t{int((picked_count/each_file_rows) * 100)}%, {picked_count} rows added; adding row: {picked_rows[0]}, time: {split_arr[0]}')

                    prev_data = (list(input_data_buffer_arr))
                    input_data_np[picked_count] = np.array(prev_data).astype(fp_type)
                    labels_np[picked_count] = label
                    picked_count += 1
                    picked_rows.popleft()

                    if len(picked_rows) == 0:
                        print(f'\t\t\tall rows picked. Next!')
                        break
            # print('\n\nREADING FNISHED!\n\n', f'line_count: {line_count}, picked_rows: {picked_rows}, label_count: {label_count}, filename: {filename}')

        np.save(input_data_outfile, input_data_np.astype(fp_type), allow_pickle=True)
        np.save(labels_outfile, np_utils.to_categorical(labels_np.astype(fp_type), 3), allow_pickle=True)
        print(f'\t\t{filename} processed, saved as np!\n')



# # Run Data Processing


def process(std_out : bool, check_empty : bool, norm_mode : str, parse_mode : str, default : bool, args : dict):
    

    # write to an outfile
    if not std_out:
        f = open(args['output_file_path']+'/output.txt', "w")
        sys.stdout = f
    
    # delete contents in dest_folder
    if check_empty:
        if (norm_mode != 'skip'):
            dest_folder = args['normalized_data_folder_path']
            if not isEmptyDir(dest_folder):
                print(f'{dest_folder} not empty, deleting all its content...')
                deleteAllInside(dest_folder)
            if not isEmptyDir(dest_folder):
                raise ValueError("{} is not empty".format(dest_folder))
        if (parse_mode != 'skip'):
            dest_folder = args['model_data_label_folder_path']
            if not isEmptyDir(dest_folder):
                print(f'{dest_folder} not empty, deleting all its content...')
                deleteAllInside(dest_folder)
            if not isEmptyDir(dest_folder):
                raise ValueError("{} is not empty".format(dest_folder))

    time_elapsed = [-1.0, -1.0]

    # Normalization
    start_time = time.time() # time.perf_counter()
    if (norm_mode == 'low_memory'):
        print('You have chosen the low_memory normalization.\nWe will split the normalization into four 3-month parts')
        start_dates = ['20210802', '20211025', '20220125', '20220425']
        end_dates = ['20211101', '20220201', '20220501', '20220802']
        for i in range(len(end_dates)):
            args['start_date'] = start_dates[i]
            args['end_date'] = end_dates[i]
            if not default: preprocess(data_folder=args["raw_data_folder_path"], dest_folder=args["normalized_data_folder_path"], mode='w', prediction_horizon=args["look_back"], label_threshold=args["label_threshold"], start_date=args["start_date"], end_date=args["end_date"], skip_5=True, fp_type=args["fp_type"])
            elif default: preprocess()
    elif (norm_mode == 'regular'):
        if not default: preprocess(data_folder=args["raw_data_folder_path"], dest_folder=args["normalized_data_folder_path"], mode='w', prediction_horizon=args["look_back"], label_threshold=args["label_threshold"], start_date=args["start_date"], end_date=args["end_date"], skip_5=True, fp_type=args["fp_type"])
        elif default: preprocess()
    elif (norm_mode == 'skip'):
        pass
    else:
        raise(f'{norm_mode} is neither \'low_memory\', nor \'regular\'')
    time_elapsed[0] = (time.time() - start_time)
    print("--- %s seconds ---" % (time.time() - start_time)) # time.perf_counter()
    

    # Making Data & Labels
    start_time = time.time() # time.perf_counter()
    if (parse_mode == 'low_memory'):
        if not default:
            lowMemParse(data_folder=args["normalized_data_folder_path"], 
                        dest_folder=args["model_data_label_folder_path"], 
                        each_file_rows=args["each_file_data_num"], 
                        look_back=args["look_back"], 
                        fp_type=args["fp_type"], 
                        input_out=f'data_{args["total_data_num"], args["look_back"]}.npy', 
                        labels_out=f'labels_{args["total_data_num"], args["look_back"]}.npy')
        elif default: lowMemParse()
    elif (parse_mode == 'regular'):
        regularParse()
    elif (parse_mode == 'skip'):
        pass
    else:
        raise(f'{parse_mode} is neither \'low_memory\', nor \'regular\'')
    time_elapsed[1] = (time.time() - start_time)
    print("--- %s seconds ---" % (time.time() - start_time)) # time.perf_counter()

    if not std_out:
        sys.stdout = sys.__stdout__
        f.close()
        print(time_elapsed)





args_each_file_data_num = 75000
args_file_num = 4

# start_date and end_date cannot be too close, as there won't be enough of data
args : dict = {"raw_data_folder_path": '/vagrant/data/book_snapshots',
               "normalized_data_folder_path": '/vagrant/data/intermediate_data/norm_data',
               "model_data_label_folder_path": '/vagrant/data/intermediate_data/model_data',
               "output_file_path": '/vagrant',
               "fp_type": np.float32,
               "each_file_data_num": args_each_file_data_num,
               "file_num": args_file_num,
               "total_data_num": int(args_file_num * args_each_file_data_num),
               "look_back": 100,
               "label_threshold": 0.000015,
               "start_date": "20210801",
               "end_date": "20210811",
}


process(std_out=True, check_empty=True, norm_mode='low_memory', parse_mode='low_memory', default=False, args=args)


process(std_out=True, check_empty=True, norm_mode='skip', parse_mode='low_memory', default=False, args=args)





