#!/bin/bash

#Set Environment Variables
export TF_CPP_MIN_LOG_LEVEL=2
#Download files
if [ -z "$(ls -A /vagrant/data/iex_downloads)" ]; then
    echo "Downloading market data..."
    /vagrant/setup/scripts/download.sh
fi

#Parse files
if [ -z "$(ls -A /vagrant/data/book_snapshots)" ]; then
    echo "Parsing market data..."
    /vagrant/setup/scripts/parse_all.sh
fi

#Preprocess files
if [ ! -f "/vagrant/data/intermediate_data/model_data/data_(300000, 100).npy" ] || [ ! -f "/vagrant/data/intermediate_data/model_data/labels_(300000, 100).npy" ] ; then
    echo "Preprocessing market data..."
    /vagrant/setup/scripts/preprocess.sh
fi

#Run algorithms
echo "All data present for models"
python3.8 /vagrant/ultimate_benchmark.py --data local --model all --mode gpu


