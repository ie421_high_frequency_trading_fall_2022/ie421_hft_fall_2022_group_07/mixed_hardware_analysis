#from google.colab import drive
from google.cloud import storage
import io

project_id = 'upbeat-sunup-372508'
# from google.colab import auth
# auth.authenticate_user()
import numpy as np

from keras.utils import np_utils
from keras.models import Model, load_model
from keras.optimizers import Adam
from keras.layers import Input, Conv2D, LeakyReLU, MaxPooling2D, concatenate, LSTM, Reshape, Dense
from keras.callbacks import EarlyStopping

import sys
import os
import tensorflow as tf
import time

# PATH = os.path.join(os.getcwd() , 'upbeat-sunup-372508-48faf8bf4bca.json')
# os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = PATH

sys.path.append(os.getcwd())

# Check for TensorFlow GPU access
#print(f"TensorFlow has access to the following devices:\n{tf.config.list_physical_devices()}")

# See TensorFlow version
#print(f"TensorFlow version: {tf.__version__}")
# def load_data_local():
#     print("Loading Data...")
#     curr = os.curdir
#     x = np.load(os.path.join(curr, 's_data_(300000, 100, 40).npy'), allow_pickle=True)
#     y = np.load(os.path.join(curr, 's_labels_(300000, 3)_100.npy'), allow_pickle=True)
#     #x = np.expand_dims(x, axis=1)
#    # print(x.shape)
#     x_train = x[:2000]
#     x_valid = x[200000 : 250000]
#     x_test = x[250000 : 300000]

#     del x

#     y_train = y[:2000]
#     y_valid = y[200000 : 250000]
#     y_test = y[250000 : 300000]

#     del y

#     print("Data Loaded")

#     return x_train, x_valid, x_test, y_train, y_valid, y_test
def get_hyperParams():
    lr = 0.05
    hyper_params = {
        'lookback_timestep' : 100,
        'feature_num' : 40,

        #Conv param
        'conv_filter_num' : 16,

        #Inception module param
        'inception_num' : 32,

        #LSTM param
        'LSTM_num' : 64,

        #Activation param
        'leaky_relu_alpha' : 0.01,

        # categorical crossentropy loss
        'loss' : 'categorical_crossentropy',

        # ADAM is used
        'learning_rate' : lr,
        'adam_epsilon' : 1,
        'optimizer' :  Adam(learning_rate=lr, epsilon=1),

        # accuracy is used for stopping training
        'metrics' : ['accuracy'],

        #max epoch num is not specified in paper, use 120 because paper mentions training stops at about 100 epochs
        'num_epoch' : 64,
        #stop training when validation accuracy does not improve for 20 epochs
        'stop_epoch_num' : 50,
        #mini-batch size 32 from paper
        'batch_size' : 512,
    }
    return hyper_params


def initiate_DeepLOB_model(lookback_timestep, feature_num, conv_filter_num, inception_num, LSTM_num, leaky_relu_alpha,
                          loss, optimizer, metrics, dtype=tf.float32):
    #with strategy.scope():
    
    input_tensor = Input(shape=(lookback_timestep, feature_num, 1), dtype=dtype, name='data')
    
    # Conv block1
    # print(input_tensor.shape)
    conv_layer1 = Conv2D(conv_filter_num, (1,2), strides=(1, 2), dtype=dtype)(input_tensor)
    conv_layer1 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer1)
    # print(conv_layer1.shape)
    conv_layer1 = Conv2D(conv_filter_num, (4,1), padding='same', dtype=dtype)(conv_layer1)
    conv_layer1 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer1)
    # print(conv_layer1.shape)
    conv_layer1 = Conv2D(conv_filter_num, (4,1), padding='same', dtype=dtype)(conv_layer1)
    conv_layer1 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer1)
    # print("conv_layer1.shape: ", conv_layer1.shape)

    # Conv block2
    conv_layer2 = Conv2D(conv_filter_num, (1,2), strides=(1, 2), dtype=dtype)(conv_layer1)
    conv_layer2 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer2)
    # print(conv_layer2.shape)
    conv_layer2 = Conv2D(conv_filter_num, (4,1), padding='same', dtype=dtype)(conv_layer2)
    conv_layer2 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer2)
    # print(conv_layer2.shape)
    conv_layer2 = Conv2D(conv_filter_num, (4,1), padding='same', dtype=dtype)(conv_layer2)
    conv_layer2 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer2)
    # print("conv_layer2.shape: ", conv_layer2.shape)

    # Conv block3
    conv_layer3 = Conv2D(conv_filter_num, (1,10), dtype=dtype)(conv_layer2)
    conv_layer3 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer3)
    # print(conv_layer3.shape)
    conv_layer3 = Conv2D(conv_filter_num, (4,1), padding='same', dtype=dtype)(conv_layer3)
    conv_layer3 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer3)
    # print(conv_layer3.shape)
    conv_layer3 = Conv2D(conv_filter_num, (4,1), padding='same', dtype=dtype)(conv_layer3)
    conv_layer3 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(conv_layer3)
    # print("conv_layer3.shape: ", conv_layer3.shape)
    
    # Inception module
    inception_module1 = Conv2D(inception_num, (1,1), padding='same', dtype=dtype)(conv_layer3)
    inception_module1 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(inception_module1)
    # print(inception_module1.shape)
    inception_module1 = Conv2D(inception_num, (3,1), padding='same', dtype=dtype)(inception_module1)
    inception_module1 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(inception_module1)
    #print("inception_module1.shape: ", inception_module1.shape)

    inception_module2 = Conv2D(inception_num, (1,1), padding='same', dtype=dtype)(conv_layer3)
    inception_module2 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(inception_module2)
    # print(inception_module2.shape)
    inception_module2 = Conv2D(inception_num, (5,1), padding='same', dtype=dtype)(inception_module2)
    inception_module2 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(inception_module2)
   # print("inception_module2.shape: ", inception_module2.shape)

    inception_module3 = MaxPooling2D((3,1), strides=(1,1), padding='same', dtype=dtype)(conv_layer3)
    # print(inception_module3.shape)
    inception_module3 = Conv2D(inception_num, (1,1), padding='same', dtype=dtype)(inception_module3)
    # print(inception_module3.shape)
    inception_module3 = LeakyReLU(alpha=leaky_relu_alpha, dtype=dtype)(inception_module3)
    #print("inception_module3.shape: ", inception_module3.shape)
    
    inception_module_final = concatenate([inception_module1, inception_module2, inception_module3], axis=3)
    # print(inception_module_final.shape)
    inception_module_final = Reshape((inception_module_final.shape[1], inception_module_final.shape[3]), dtype=dtype)(inception_module_final)
    # print("inception_module_final.shape: ", inception_module_final.shape)

    # LSTM
    LSTM_output = LSTM(LSTM_num, dtype=dtype)(inception_module_final)
    # print("LSTM_output.shape: ", LSTM_output.shape)

    # Fully Connected Layer with softmax activation function for output
    model_output = Dense(3, activation='softmax', dtype=dtype)(LSTM_output)
    # print("model_output.shape: ", model_output.shape)
    
    DeepLOB_model = Model(inputs=input_tensor, outputs= model_output)  
    
    DeepLOB_model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    return DeepLOB_model

class TensorFlow():
    def load_data(self, x_train, x_valid, x_test, y_train, y_valid, y_test):
        self.x_train = np.squeeze(x_train, axis=1)
        self.x_valid = np.squeeze(x_valid, axis=1)
        self.x_test = np.squeeze(x_test, axis=1)
        self.y_train = y_train
        self.y_valid = y_valid
        self.y_test = y_test
    def train(self):
        hp = get_hyperParams()
        DeepLOB_model = initiate_DeepLOB_model(
        hp['lookback_timestep'], hp['feature_num'], hp['conv_filter_num'], 
        hp['inception_num'], hp['LSTM_num'], hp['leaky_relu_alpha'], hp['loss'], hp['optimizer'], hp['metrics']
        )
        #DeepLOB_model.summary()
        es = EarlyStopping(monitor='val_accuracy', mode='max', patience = hp['stop_epoch_num'], verbose=0)
        start = time.perf_counter()
        DeepLOB_model.fit(self.x_train, self.y_train, epochs=hp['num_epoch'], batch_size=hp['batch_size'], verbose=0, validation_data=(self.x_valid, self.y_valid), callbacks = [es])
        end = time.perf_counter()
        print("\n\tTensorflow Train Time: " ,end - start, " seconds")
        #DeepLOB_model.save('/content/drive/MyDrive/IE421/models/tf_DeepLOB_fp32.h5')
        self.model = DeepLOB_model

    def test(self):
        start = time.perf_counter()
        loss, accuracy = self.model.evaluate(self.x_test, self.y_test, verbose=0, batch_size=512)
        end = time.perf_counter()
        print("\n\tTensorflow Infrence Time: " ,end - start, " seconds")
        return loss, accuracy

# x_train, x_valid, x_test, y_train, y_valid, y_test = load_data_local()

# TF = TensorFlow()
# TF.load_data(x_train, x_valid, x_test, y_train, y_valid, y_test)

# TF.train()

# del x_train
# del x_valid
# del y_train
# del y_valid

# load, accuracy = TF.tf_test()
