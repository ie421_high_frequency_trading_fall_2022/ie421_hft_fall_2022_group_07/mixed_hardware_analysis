import os
import glob
import random
import numpy as np

import cv2

import caffe
from caffe.proto import caffe_pb2

def make_datum(img, label):
    #image is numpy.ndarray format. BGR instead of RGB
    return caffe_pb2.Datum(
        channels=3,
        width=IMAGE_WIDTH,
        height=IMAGE_HEIGHT,
        label=label,
        data=np.rollaxis(img, 2).tostring())


# Main method to train and test algorithm
def caffe_benchmark():
    pass