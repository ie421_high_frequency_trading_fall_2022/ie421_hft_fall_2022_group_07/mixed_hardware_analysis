import tensorflow as tf
import coremltools
import numpy as np
import os
import time

def coreml_convert():
    DeepLOB_model = tf.keras.models.load_model('/Users/johnhofmann/Developer/Mac_ML_Benchmark/tf_DeepLOB_fp32.h5')
    DeepLOB_model.load_weights('/Users/johnhofmann/Developer/Mac_ML_Benchmark/tf_DeepLOB_fp32.h5')
    coreml_model = coremltools.convert(DeepLOB_model, minimum_deployment_target=coremltools.target.macOS13, compute_precision=coremltools.precision.FLOAT32, compute_units=coremltools.ComputeUnit.ALL,
        inputs=[coremltools.TensorType(shape=[1, 100, 40, 1], dtype=np.float32)], outputs=[coremltools.TensorType(dtype=np.float32)])
    coreml_model.save('/Users/johnhofmann/Developer/Mac_ML_Benchmark/model_float16.mlpackage')
    #return coreml_model
    #coreml_model.save("model.mlmodel")

def cdRootDir():
    cur_dir = os.getcwd()
    while cur_dir != '/':
        os.chdir('../')
        cur_dir = os.getcwd()

def load_data(new_permute : bool):
    cdRootDir()

    if new_permute:
        data_file = open('/content/drive/MyDrive/IE421/IEX_DATA/fp32/model_data/data_(300000, 100, 40).npy', 'rb')
        labels_file = open('/content/drive/MyDrive/IE421/IEX_DATA/fp32/model_data/labels_(300000, 3)_100.npy', 'rb')

        x_a = np.load(data_file, allow_pickle=True)
        x_b = np.load(data_file, allow_pickle=True)
        x_c = np.load(data_file, allow_pickle=True)
        x_d = np.load(data_file, allow_pickle=True)

        # y_a = np.load(labels_file, allow_pickle=True)
        # y_b = np.load(labels_file, allow_pickle=True)
        # y_c = np.load(labels_file, allow_pickle=True)
        # y_d = np.load(labels_file, allow_pickle=True)

        x = (np.concatenate((x_a, x_b, x_c, x_d), axis=0))
        # y = (np.concatenate((y_a, y_b, y_c, y_d), axis=0))
        y = np.load(labels_file, allow_pickle=True)

        # Generate a permuted sequence of the specified length
        permuted = np.random.permutation(300000)

        # Shuffle the data and labels using the permuted sequence
        x = x[permuted]
        y = y[permuted]

        # not necessary, but on colab, you need them
        del x_a
        del x_b
        del x_c
        del x_d
        # del y_a
        # del y_b
        # del y_c
        # del y_d

        x[:200000], x[200000 : 250000], x[250000 : 300000], y[:200000], y[200000 : 250000], y[250000 : 300000],
    elif not new_permute:
        data_file = open('/Users/johnhofmann/Developer/Mac_ML_Benchmark/data.npy', 'rb')
        labels_file = open('/Users/johnhofmann/Developer/Mac_ML_Benchmark/labels.npy', 'rb')

        x = np.load(data_file, allow_pickle=True)
        y = np.load(labels_file, allow_pickle=True)
        print(f'x.shape: {x.shape}, y.shape: {y.shape}')

        return x[:200000], x[200000 : 250000], x[250000 : 300000], y[:200000], y[200000 : 250000], y[250000 : 300000],

x_train, x_valid, x_test, y_train, y_valid, y_test = load_data(False)

x_test.astype(np.float32).tofile('/Users/johnhofmann/Developer/Mac_ML_Benchmark/coreML_data')
multi_array = coremltools.models.MLMultiArray(x_train)

# save the multiArray to a file
with open("/Users/johnhofmann/Developer/Mac_ML_Benchmark/coreML_mac_data.mlmultiarray", "wb") as f:
    f.write(multi_array.SerializeToString())

def coreml_test():
    #model = coreml_convert()
    #print(model.get_spec())
    coreml_convert()
    #print(x_test.shape)
    #x_test = x_test.reshape(-1, 100, 40, 1)
    # x_temp = x_test[0]
    # x_temp = x_temp.reshape(-1, 100, 40, 1)

    start = time.time()
    # predictions = model.predict({'input_1' : x_test[0 : 10000]})
    # predictions = model.predict({'input_1' : x_test[10001 : 20000]})
    # predictions = model.predict({'input_1' : x_test[20001 : 30000]})
    # predictions = model.predict({'input_1' : x_test[30001 : 40000]})
    # predictions = model.predict({'input_1' : x_test[40001 : 50000]})
    end = time.time()

    print("Prediction time: ", end - start)

coreml_test()