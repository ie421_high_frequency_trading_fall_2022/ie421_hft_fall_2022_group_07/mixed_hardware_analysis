# -*- coding: utf-8 -*-

# load packages
import numpy as np
from datetime import datetime
from tqdm import tqdm  # progress bar
import torch
import torch.nn as nn
from google.cloud import storage
import io
import sys
import os
import time

# Use GPU if exists
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Model Parameters
lr = 0.05
lookback_timestep = 100
feature_num = 40
conv_filter_num = 16  # Conv param
inception_num = 32  # Inception module param
LSTM_num = 64  # LSTM param
leaky_relu_alpha = 0.01  # Activation param
batch_size = 512
# max epoch num is not specified in paper use 120 because paper mentions training stops at about 100 epochs
num_epoch = 64
# stop training when validation accuracy does not improve for 20 epochs
stop_epoch_num = 50
# mini-batch size 32 from paper

"""# Data preparation

We used no auction dataset that is normalised by decimal precision approach in their work. The first seven days are training data and the last three days are testing data. A validation set (20%) from the training set is used to monitor the overfitting behaviours.  

The first 40 columns of the FI-2010 dataset are 10 levels ask and bid information for a limit order book and we only use these 40 features in our network. The last 5 columns of the FI-2010 dataset are the labels with different prediction horizons. 
"""


class MyDataset(torch.utils.data.Dataset):
    def __init__(self, data, labels):
        self.data = data
        self.labels = labels

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        x = self.data[idx]
        y = self.labels[idx]
        return x, y


"""# Model Architecture

Please find the detailed discussion of our model architecture in our paper.
"""


class deeplob(nn.Module):
    def __init__(self, y_len, conv_filter_num, inception_num, LSTM_num):
        super().__init__()
        self.y_len = y_len
        self.inception_num = inception_num
        self.LSTM_num = LSTM_num
        # print(1)
        # convolution blocks
        self.conv1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=conv_filter_num,
                      kernel_size=(1, 2), stride=(1, 2)),
            nn.LeakyReLU(negative_slope=0.01),
            #             nn.Tanh(),
            nn.BatchNorm2d(conv_filter_num),
            nn.Conv2d(in_channels=conv_filter_num,
                      out_channels=conv_filter_num, kernel_size=(4, 1)),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(conv_filter_num),
            nn.Conv2d(in_channels=conv_filter_num,
                      out_channels=conv_filter_num, kernel_size=(4, 1)),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(conv_filter_num),
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(in_channels=conv_filter_num, out_channels=conv_filter_num,
                      kernel_size=(1, 2), stride=(1, 2)),
            nn.Tanh(),
            nn.BatchNorm2d(conv_filter_num),
            nn.Conv2d(in_channels=conv_filter_num,
                      out_channels=conv_filter_num, kernel_size=(4, 1)),
            nn.Tanh(),
            nn.BatchNorm2d(conv_filter_num),
            nn.Conv2d(in_channels=conv_filter_num,
                      out_channels=conv_filter_num, kernel_size=(4, 1)),
            nn.Tanh(),
            nn.BatchNorm2d(conv_filter_num),
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(in_channels=conv_filter_num,
                      out_channels=conv_filter_num, kernel_size=(1, 10)),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(conv_filter_num),
            nn.Conv2d(in_channels=conv_filter_num,
                      out_channels=conv_filter_num, kernel_size=(4, 1)),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(conv_filter_num),
            nn.Conv2d(in_channels=conv_filter_num,
                      out_channels=conv_filter_num, kernel_size=(4, 1)),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(conv_filter_num),
        )

        # inception moduels
        self.inp1 = nn.Sequential(
            nn.Conv2d(in_channels=conv_filter_num, out_channels=inception_num,
                      kernel_size=(1, 1), padding='same'),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(inception_num),
            nn.Conv2d(in_channels=inception_num, out_channels=inception_num,
                      kernel_size=(3, 1), padding='same'),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(inception_num),
        )
        self.inp2 = nn.Sequential(
            nn.Conv2d(in_channels=conv_filter_num, out_channels=inception_num,
                      kernel_size=(1, 1), padding='same'),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(inception_num),
            nn.Conv2d(in_channels=inception_num, out_channels=inception_num,
                      kernel_size=(5, 1), padding='same'),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(inception_num),
        )
        self.inp3 = nn.Sequential(
            nn.MaxPool2d((3, 1), stride=(1, 1), padding=(1, 0)),
            nn.Conv2d(in_channels=conv_filter_num, out_channels=inception_num,
                      kernel_size=(1, 1), padding='same'),
            nn.LeakyReLU(negative_slope=0.01),
            nn.BatchNorm2d(inception_num),
        )

        # lstm layers
        self.lstm = nn.LSTM(input_size=inception_num*3,
                            hidden_size=LSTM_num, num_layers=1, batch_first=True)
        self.fc1 = nn.Linear(LSTM_num, self.y_len)

    def forward(self, x):
        # h0: (number of hidden layers, batch size, hidden size)
        h0 = torch.zeros(1, x.size(0), self.LSTM_num).to(device)
        c0 = torch.zeros(1, x.size(0), self.LSTM_num).to(device)

        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)

        x_inp1 = self.inp1(x)
        x_inp2 = self.inp2(x)
        x_inp3 = self.inp3(x)

        x = torch.cat((x_inp1, x_inp2, x_inp3), dim=1)
#         x = torch.transpose(x, 1, 2)
        x = x.permute(0, 2, 1, 3)
        x = torch.reshape(x, (-1, x.shape[1], x.shape[2]))

        x, _ = self.lstm(x, (h0, c0))
        x = x[:, -1, :]
        x = self.fc1(x)
        forecast_y = torch.softmax(x, dim=1)
        return forecast_y




"""# Model Training

"""

# A function to encapsulate the training loop


def batch_gd(model, criterion, optimizer, train_loader, test_loader, epochs):
    train_losses = np.zeros(epochs)
    test_losses = np.zeros(epochs)
    best_test_loss = np.inf
    best_test_epoch = 0
    for it in tqdm(range(epochs)):
        model.train()
        t0 = datetime.now()
        train_loss = []
        for inputs, targets in train_loader:
            # move data to GPU
            inputs, targets = inputs.to(device, dtype=torch.float), targets.to(
                device, dtype=torch.float)
            # print("inputs.shape:", inputs.shape)
            # zero the parameter gradients
            optimizer.zero_grad()
            # Forward pass
            # print("about to get model output")
            outputs = model(inputs)
            # print("done getting model output")
            # print("outputs.shape:", outputs.shape, "targets.shape:", targets.shape)
            loss = criterion(outputs, targets)
            # Backward and optimize
            # print("about to optimize")
            loss.backward()
            optimizer.step()
            train_loss.append(loss.item())
        # Get train loss and test loss
        train_loss = np.mean(train_loss)  # a little misleading

        model.eval()
        test_loss = []
        for inputs, targets in test_loader:
            inputs, targets = inputs.to(device, dtype=torch.float), targets.to(
                device, dtype=torch.float)
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            test_loss.append(loss.item())
        test_loss = np.mean(test_loss)

        # Save losses
        train_losses[it] = train_loss
        test_losses[it] = test_loss

        if test_loss < best_test_loss:
            # torch.save(model, './best_val_model_pytorch')
            best_test_loss = test_loss
            best_test_epoch = it
            # print('model could be saved')

        dt = datetime.now() - t0
        print(f'Epoch {it+1}/{epochs}, Train Loss: {train_loss:.4f}, \
          Validation Loss: {test_loss:.4f}, Duration: {dt}, Best Val Epoch: {best_test_epoch}')

    return train_losses, test_losses




model = deeplob(3, conv_filter_num, inception_num, LSTM_num)
model.to(device)

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=lr, eps=10e-3)

class PyTorch():
    def load_data(self, x_train, x_valid, x_test, y_train, y_valid, y_test):
        # print(device)
        # print(torch.__version__)
        dataset_train = MyDataset(torch.from_numpy(x_train), torch.from_numpy(y_train))
        dataset_val = MyDataset(torch.from_numpy(x_valid), torch.from_numpy(y_valid))
        dataset_test = MyDataset(torch.from_numpy(x_test), torch.from_numpy(y_test))
        self.train_loader = torch.utils.data.DataLoader(
            dataset=dataset_train, batch_size=batch_size, shuffle=True)
        self.val_loader = torch.utils.data.DataLoader(
            dataset=dataset_val, batch_size=batch_size, shuffle=False)
        self.test_loader = torch.utils.data.DataLoader(
            dataset=dataset_test, batch_size=batch_size, shuffle=False)

        # Testing data size
        self.tmp_loader = torch.utils.data.DataLoader(
            dataset=dataset_train, batch_size=1, shuffle=True)
        
    def train(self):
        start = time.perf_counter()
        
        train_losses, val_losses = batch_gd(
            model, criterion, optimizer, self.train_loader, self.val_loader, epochs=num_epoch)
        end = time.perf_counter()
        seconds = end - start
        print("\n\tPytorch Inference Time: ", seconds, " seconds")
    def test(self):
        start_test = time.perf_counter()
        n_correct = 0.
        n_total = 0.
        for inputs, targets in self.test_loader:
            # Move to GPU
            inputs, targets = inputs.to(device, dtype=torch.float), targets.to(
                device, dtype=torch.float)

            # Forward pass
            outputs = model(inputs)
            # print(outputs)
            # print(outputs.shape)

            # Get prediction
            # torch.max returns both max and argmax
            _, predictions = torch.max(outputs, 1)
            # print(predictions.shape)
            # print(targets.shape)
            # print(targets)
            _, targetsMax = torch.max(targets, 1)

            # update counts
            n_correct += (predictions == targetsMax).sum().item()
            n_total += targetsMax.shape[0]

        test_acc = n_correct / n_total
        print(f"Test acc: {test_acc:.4f}")
        end_test = time.perf_counter()
        seconds = end_test - start_test
        print("\n\tPytorch Test Time: ", seconds, " seconds")
