# from sklearn.model_selection import train_test_split
# from google.cloud import storage
# #from google.colab import drive
import time
import mxnet as mx
from mxnet import gluon
from mxnet.gluon import nn
from mxnet.gluon import rnn
from mxnet import autograd as ag
import numpy as np
import os

curr = os.getcwd()
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(curr, 'creds.json')
#npx.set_np()
#print('set np')
#drive.mount('/content/drive', force_remount=True)

learning_rate = 0.05
dtype='float32'
lookback_timestep = 100
feature_num = 40

#Conv param
conv_filter_num = 16

#Inception module param
inception_num = 32

#LSTM param
LSTM_num = 64

#Activation param
leaky_relu_alpha = 0.01

batch_size = 512

#max epoch num is not specified in paper use 120 because paper mentions training stops at about 100 epochs
num_epoch = 64
#stop training when validation accuracy does not improve for 20 epochs
stop_epoch_num = 50
#mini-batch size 32 from paper

import mxnet.ndarray as F

class Net(gluon.Block):
    def __init__(self, **kwargs):
        super(Net, self).__init__(**kwargs)
        with self.name_scope():
            # layers created in name_scope will inherit name space
            # from parent layer.
               #with strategy.scope():

         # input_tensor = nn.Input(shape=(lookback_timestep, feature_num, 1) , name='data')

                # Conv block1
                # print(input_tensor.shape)
                self.conv_blocks = nn.Sequential()
                self.conv_blocks.add(
                # padding= ((kernel_size[0]-1)//2, (kernel_size[1]-1)//2)
                nn.Conv2D(conv_filter_num, (1,2), strides=(1,2), padding=(0,0)),
                nn.LeakyReLU(alpha=leaky_relu_alpha),
                # print(conv_layer1.shape)
                nn.Conv2D(conv_filter_num, (4,1), padding=(1,0)),
                nn.LeakyReLU(alpha=leaky_relu_alpha),
                # print(conv_layer1.shape)
                nn.Conv2D(conv_filter_num, (4,1), padding=(2,0)),
                nn.LeakyReLU(alpha=leaky_relu_alpha),
                # print("conv_layer1.shape: ", conv_layer1.shape)

                # # Conv block2
                nn.Conv2D(conv_filter_num, (1,2), strides=(1, 2)),
                nn.LeakyReLU(alpha=leaky_relu_alpha),
                # print(conv_layer2.shape,
                nn.Conv2D(conv_filter_num, (4,1), padding=(1,0)),
                nn.LeakyReLU(alpha=leaky_relu_alpha),
                # print(conv_layer2.shape)
                nn.Conv2D(conv_filter_num, (4,1), padding=(2,0)),
                nn.LeakyReLU(alpha=leaky_relu_alpha),
                # print("conv_layer2.shape: ", conv_layer2.shape)

                # # Conv block3
                nn.Conv2D(conv_filter_num, (1,10)),
                nn.LeakyReLU(alpha=leaky_relu_alpha),
                # print(conv_layer3.shape)
                nn.Conv2D(conv_filter_num, (4,1), padding=(1,0)),
                nn.LeakyReLU(alpha=leaky_relu_alpha),
                # print(conv_layer3.shape)
                nn.Conv2D(conv_filter_num, (4,1), padding=(2,0)),
                nn.LeakyReLU(alpha=leaky_relu_alpha)
                )
                # Inception module
                self.inception_module1 = nn.Sequential()
                #   self.inception_module1.add(self.conv_blocks)
                self.inception_module1.add(nn.Conv2D(inception_num, (1,1)))
                self.inception_module1.add(nn.LeakyReLU(alpha=leaky_relu_alpha))
                # print(inception_module1.shape)
                self.inception_module1.add(nn.Conv2D(inception_num, (3,1), padding=(1,0)))
                self.inception_module1.add(nn.LeakyReLU(alpha=leaky_relu_alpha))
                # print("inception_module1.shape: ", inception_module1.shape)

                self.inception_module2 = nn.Sequential()
                #   self.inception_module2.add(self.conv_blocks)
                self.inception_module2.add(nn.Conv2D(inception_num, (1,1)))
                self.inception_module2.add(nn.LeakyReLU(alpha=leaky_relu_alpha))
                # print(inception_module2.shape)
                self.inception_module2.add(nn.Conv2D(inception_num, (5,1), padding=(2,0)))
                self.inception_module2.add(nn.LeakyReLU(alpha=leaky_relu_alpha))
                # print("inception_module2.shape: ", incep)

                self.inception_module3 = nn.Sequential()
                #   self.inception_module3.add(self.conv_blocks)
                self.inception_module3.add(nn.MaxPool2D(pool_size=(3,1), strides=(1,1), padding=(1,0)))
                # print(inception_module3.shape)
                self.inception_module3.add(nn.Conv2D(inception_num, (1,1))) 
                self.inception_module3.add(nn.LeakyReLU(alpha=leaky_relu_alpha))
                # print("inception_module3.shape: ", inception_module3.shape)
                # print("inception_module_final.shape: ", inception_module_final.shape)

                # LSTM
                self.LSTM = rnn.LSTM(LSTM_num)
                # print("LSTM_output.shape: ", LSTM_output.shape)

                # Fully Connected Layer with softmax activation function for output
                self.final = nn.Dense(3)

                # print("model_output.shape: ", model_output.shape)

                # DeepLOB_model = Model(inputs=input_tensor, outputs= model_output)  

                # DeepLOB_model.compile(optimizer=optimizer, loss=loss, metrics=metrics)


    def forward(self, x):
        #print(type(x))
       # print(x.shape, x.dtype, 'input')
        x = self.conv_blocks(x)
        #print(x.shape, 'conv')
        inception_module_1 = self.inception_module1(x)
        inception_module_2 = self.inception_module2(x)
        inception_module_3 = self.inception_module3(x)
        #print(type(inception_module_1), 'm1')
        #print(inception_module_1.shape, inception_module_2.shape, inception_module_3.shape)
        # print(sys.getsizeof(inception_module_1))
        # print(inception_module_1.size, inception_module_1.values.itemsize, inception_module_1.size * inception_module_1.values.itemsize)
        #print('it1')
        # inception_module_final = np.zeros((batch_size, 96, 100, 1))
        # # for row in inception_module_final:
        # #     print(row[:32].shape, inception_module_1.shape)
        # #     row[:32] = inception_module_1
        # print(type(inception_module_1))
        # print(inception_module_final[:,:32].shape, inception_module_1.shape)
        # inception_module_final[:,:32] = inception_module_1
        inception_module_final = F.concat(inception_module_1, inception_module_2, inception_module_3, dim=1)
        #print(inception_module_final)
        #print(inception_module_final.shape, 'concat')
        inception_module_final = F.reshape(inception_module_final, (batch_size, inception_module_final.shape[1], inception_module_final.shape[2]))
        #print(inception_module_final.shape)
        #print('reshape')
        LSTM_output = self.LSTM(inception_module_final)
        #print(LSTM_output.shape)
        #print('lstm')
        output = self.final(LSTM_output)
        #print(output)
        return output



class MXNet():
    # Run any data preprocessing steps here
    def load_data(self, x_train, x_valid, x_test, y_train, y_valid, y_test):
        y_train = np.argmax(y_train, axis=1)
        y_test = np.argmax(y_test, axis=1)
        self.train_data = mx.io.NDArrayIter(x_train, y_train, batch_size, shuffle=True)
        self.val_data = mx.io.NDArrayIter(x_test, y_test, batch_size)
        self.net = Net()
    def train_helper(self):
      ctx = [mx.gpu() if mx.test_utils.list_gpus() else mx.cpu()]
      self.net.initialize(mx.init.Xavier(magnitude=2.24), ctx=ctx)
      optimizer =  mx.optimizer.Adam(learning_rate=0.01, epsilon=10e-8)
      trainer = gluon.Trainer(self.net.collect_params(), optimizer)
      metric = mx.metric.Accuracy()
      ce = mx.metric.CrossEntropy()
      softmax_cross_entropy_loss = gluon.loss.SoftmaxCrossEntropyLoss()

      for i in range(num_epoch):
          # Reset the train data iterator.
          first = True
          self.train_data.reset()
          # Loop over the train data iterator.
          for batch in self.train_data:
              # Splits train data into multiple slices along batch_axis
              # and copy each slice into a context.
              data = gluon.utils.split_and_load(batch.data[0], ctx_list=ctx, batch_axis=0)
              # Splits train labels into multiple slices along batch_axis
              # and copy each slice into a context.
              label = gluon.utils.split_and_load(batch.label[0], ctx_list=ctx, batch_axis=0)
              outputs = []
              # Inside training scope
              with ag.record():
                  for x, y in zip(data, label):
                      
                      z = self.net(x)
                    #   if first:
                    #     print(z, y)
                    #     first = False
                     # print(z, y)
                      # Computes softmax cross entropy loss.
                      loss = softmax_cross_entropy_loss(z, y)
                      # Backpropogate the error for one iteration.
                      loss.backward()
                      outputs.append(z)
              # Updates internal evaluation
              metric.update(label, outputs)
            #   ce.update(label, outputs)
              # Make one step of parameter update. Trainer needs to know the
              # batch size of data to normalize the gradient by 1/batch_size.
              trainer.step(batch.data[0].shape[0])
          # Gets the evaluation result.
          name, acc = metric.get()
          # Reset evaluation result to initial state.
          metric.reset()
          #print(self.net.collect_params())
          print('training acc at epoch %d: %s=%f'%(i, name, acc))
        #   name, acc = ce.get()
        #   # Reset evaluation result to initial state.
        #   ce.reset()
        #   print('training loss at epoch %d: %s=%f'%(i, name, acc))
    def train(self):
        start = time.perf_counter()
        self.train_helper()
        end = time.perf_counter()
        seconds = end - start
        print("\n\tMXnet Train Time: " ,seconds, " seconds")
        return seconds
    def test_helper(self):
        ctx = [mx.gpu() if mx.test_utils.list_gpus() else mx.cpu()]
        # Use Accuracy as the evaluation metric.
        metric = mx.metric.Accuracy()
        # Reset the validation data iterator.
        self.val_data.reset()
        # Loop over the validation data iterator.
        for batch in self.val_data:
            # Splits validation data into multiple slices along batch_axis
            # and copy each slice into a context.
            data = gluon.utils.split_and_load(batch.data[0], ctx_list=ctx, batch_axis=0)
            # Splits validation label into multiple slices along batch_axis
            # and copy each slice into a context.
            label = gluon.utils.split_and_load(batch.label[0], ctx_list=ctx, batch_axis=0)
            outputs = []
            for x in data:
                outputs.append(self.net(x))
            # Updates internal evaluation
            metric.update(label, outputs)
        print('validation acc: %s=%f' % metric.get())
    def test(self):
        start = time.perf_counter()
        self.test_helper()
        end = time.perf_counter()
        seconds = end - start
        print("\n\tMXNet Infrence Time: " ,seconds, " seconds")
        return seconds

# MX = MXNet()
# MX.load_data(x_train, x_valid, x_test, y_train, y_valid, y_test)
# MX.train()
# MX.test()
# MX.test()
# MX.test()