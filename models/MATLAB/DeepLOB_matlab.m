numFeatures = 12;
numHiddenUnits = 100;
numClasses = 9;


%dummy data
load WaveformData
data(1:5)
numChannels = size(data{1},1);
figure
tiledlayout(2,2)
for i = 1:4
    nexttile
    stackedplot(data{i}')

    xlabel("Time Step")
end

%data Processing
numObservations = numel(data);
idxTrain = 1:floor(0.9*numObservations);
idxTest = floor(0.9*numObservations)+1:numObservations;
dataTrain = data(idxTrain);
dataTest = data(idxTest);

for n = 1:numel(dataTrain)
    X = dataTrain{n};
    XTrain{n} = X(:,1:end-1);
    TTrain{n} = X(:,2:end);
end

muX = mean(cat(2,XTrain{:}),2);
sigmaX = std(cat(2,XTrain{:}),0,2);

muT = mean(cat(2,TTrain{:}),2);
sigmaT = std(cat(2,TTrain{:}),0,2);

for n = 1:numel(XTrain)
    XTrain{n} = (XTrain{n} - muX) ./ sigmaX;
    TTrain{n} = (TTrain{n} - muT) ./ sigmaT;
end

%Define Hyper Parameters
lookback_timestep = 100;
feature_num = 40;

%Conv param
conv_filter_num = 16;

%Inception module param
inception_num = 32;

%LSTM param
LSTM_num = 64;

%Activation param
leaky_relu_alpha = 0.01;

% categorical crossentropy loss
loss = 'categorical_crossentropy';

% ADAM is used
learning_rate = 0.01;
adam_epsilon = 1;
%optimizer = Adam(lr=learning_rate, epsilon=1);

% accuracy is used for stopping training
metrics = 'accuracy';

%max epoch num is not specified in paper, use 120 because paper mentions training stops at about 100 epochs
num_epoch = 120;
%stop training when validation accuracy does not improve for 20 epochs
stop_epoch_num = 20;
%mini-batch size 32 from paper
batch_size = 32;

% Construct Architecture
layers = initiate_DeepLOB_model(lookback_timestep, feature_num, ...
    conv_filter_num, inception_num, LSTM_num, leaky_relu_alpha);

% Specify Training Options 
options = trainingOptions("adam", "MaxEpochs",num_epoch, MiniBatchSize=batch_size, ...
    InitialLearnRate=learning_rate);

% Train Neural Network
net = trainNetwork(XTrain, TTrain, layers, options);

% Test Network
for n = 1:size(dataTest,1)
    X = dataTest{n};
    XTest{n} = (X(:,1:end-1) - muX) ./ sigmaX;
    TTest{n} = (X(:,2:end) - muT) ./ sigmaT;
end
YTest = predict(net,XTest,SequencePaddingDirection="left");
for i = 1:size(YTest,1)
    rmse(i) = sqrt(mean((YTest{i} - TTest{i}).^2,"all"));
end



% All below are functions to define CNN LSTM Network Architecture
% Convolutional Block
function [convBlock]...
    = conv_Block(filter_size, conv_filter_num, leaky_relu_alpha)
    convBlock = [ ...
    convolution2dLayer(filter_size, ...
    conv_filter_num, 'Stride',[1,2])
    leakyReluLayer(leaky_relu_alpha)
    convolution2dLayer(filter_size, ...
    conv_filter_num, 'Padding', 'same')
    leakyReluLayer(leaky_relu_alpha)
    convolution2dLayer(filter_size, ...
    conv_filter_num, 'Padding', 'same')
    leakyReluLayer(leaky_relu_alpha)
    ];
end

function [inceptMod]...
    = incept_Mod(inceptNum, filter_size, leaky_relu_alpha)
    inceptMod = [ ...
        convolution2dLayer(filter_size, ...
        inceptNum, 'Padding', 'same')
        leakyReluLayer(scale=leaky_relu_alpha)
        ];
end

function [inceptMod_1]...
    = incept_Mod_1(inceptNum, filter_size_1, filter_size_2, ...
    leaky_relu_alpha)
    inceptMod_1 = [...
        incept_Mod(inceptNum, filter_size_1, leaky_relu_alpha)
        incept_Mod(inceptNum, filter_size_2, leaky_relu_alpha)
        ];
end

function [inceptMod_2]...
    = incept_Mod_2(inceptNum, filter_size_1, filter_size_2, ...
    leaky_relu_alpha)
    inceptMod_2 = [...
        incept_Mod(inceptNum, filter_size_1, leaky_relu_alpha)
        incept_Mod(inceptNum, filter_size_2, leaky_relu_alpha)
        ];
end

function [inceptMod_3]...
    = incept_Mod_3(inceptNum, filter_size_1, ...
    leaky_relu_alpha, poolSize, stride)
    inceptMod_3 = [...
        maxPooling2dLayer(poolSize, 'Stride',stride, 'Padding', 'same')
        incept_Mod(inceptNum, filter_size_1, leaky_relu_alpha)
        ];
end

function [layers]...
    = conv_out(lookback_timestep, feature_num, ...
    conv_filter_num, leaky_relu_alpha)
    layers =  [ ...
    
    conv_Block([1,2],conv_filter_num, leaky_relu_alpha)
    conv_Block([1,2],conv_filter_num, leaky_relu_alpha)
    conv_Block([1,10],conv_filter_num, leaky_relu_alpha)
    ];
end

function [final]...
    = initiate_DeepLOB_model(lookback_timestep, feature_num, ...
    conv_filter_num, inception_num, LSTM_num, leaky_relu_alpha)
    % loss, optimizer, metrics
    final = [ ...
        sequenceInputLayer([lookback_timestep, feature_num, 1])
        conv_out(lookback_timestep, feature_num, ...
        conv_filter_num, leaky_relu_alpha)
        incept_Mod_1(inception_num, [1,1], [3,1], leaky_relu_alpha)

        conv_out(lookback_timestep, feature_num, ...
        conv_filter_num, leaky_relu_alpha)
        incept_Mod_2(inception_num, [1,1], [5,1], leaky_relu_alpha)

        conv_out(lookback_timestep, feature_num, ...
        conv_filter_num, leaky_relu_alpha)
        incept_Mod_3(inception_num, [1,1], leaky_relu_alpha, [3,1], [1,1])
        
        lstmLayer(LSTM_num)
        fullyConnectedLayer(3)
        softmaxLayer
        classificationLayer
        ];
end