# The highest level in benchmarking logic
# Does not implement any specific model's training/inference
# It simply calls the training_benchmark and inference_benchmark functions in the model's file

# import os
import sys
import time
import argparse

import numpy as np

# sys.path.append('/vagrant/algos')
from models.mxnet.mxnet_benchmark import MXNet
from models.pytorch.pytorch_benchmark import PyTorch
from models.tensorflow.DeepLOB_fp32 import TensorFlow

from utils import load_data_local, load_data_cloud

### It includes save model time
def benchmark(args):
    model_benchmark : dict[str, list[function]] = {"pytorch": PyTorch(), "mxnet": MXNet(), 'tensorflow': TensorFlow()}
    if args.data == 'cloud':
        print('Downloading data...')
    x_train, x_valid, x_test, y_train, y_valid, y_test = load_data_local() if args.data == 'local' else load_data_cloud()

    if 'all' in args.model:
        print('Benchmarking all models:')
        for model_name, model_object in model_benchmark.items():
            print('Benchmarking model: %s' % model_name)
            model_object.load_data(x_train, x_valid, x_test, y_train, y_valid, y_test)
            print('Training...')
            # train function prints the time to train and accuracy metrics
            model_object.train()


            print('Testing...')
            model_object.test()
            print('\n')
    else:
        for model_name in args.model:
            print('Benchmarking model: %s' % model_name)

            model_object = model_benchmark[model_name]
            model_object.load_data(x_train, x_valid, x_test, y_train, y_valid, y_test)
            print('Training...')
            model_object.train()

            print('Testing...')
            model_object.test()
            print('\n')



if __name__ == "__main__":
    # make argument parser
    parser = argparse.ArgumentParser(description='IE 421 Hardware Benchmarking Tool')
    parser.add_argument(
        '--model', dest='model', nargs='+',type=str, default="all", 
        choices=['all', 'tensorflow', 'pytorch', 'mxnet'],
        help='choose which model to run'
    )
    parser.add_argument(
        '--mode', type=str, default="gpu", 
        choices=['gpu', 'cpu'],
        help='run on the cpu or gpu'
    )
    parser.add_argument(
        '--data', type=str, default="local", 
        choices=[ 'local', 'cloud'],
        help='use the data from the cloud or locally'
    )
    args = parser.parse_args()

   # print(args)

    benchmark(args)