import os
from google.cloud import storage
import io
import json
import numpy as np

def cdRootDir():
    cur_dir = os.getcwd()
    while cur_dir != '/':
        os.chdir('../')
        cur_dir = os.getcwd()

# file_path = file_name
def writeJsonFile(file_path, json_string):
    # write to a json file
    # Directly from dictionary
    cdRootDir()
    with open(file_path, 'w') as outfile:
        json.dump(json_string, outfile)

# read;
def readJsonFile(file_path):
    # read form a json file
    cdRootDir()
    with open(file_path) as json_file:
        data = json.load(json_file)
        return data

# increasing ordered by name
def getAllFileNames(folder_path) -> list:
    cdRootDir()
    data_filenames : object  = os.scandir(folder_path)
    data_filename_list : list = []
    for entry in data_filenames:
        if (entry.is_file()):
            data_filename_list.append(entry.name)
    data_filename_list.sort()
    return data_filename_list

def prevDay(cur_time : str, n : int):
    from datetime import datetime
    cur_datetime = datetime.strptime(cur_time[:10], ("%Y-%m-%d"))
    return_datetime = cur_datetime - datetime.timedelta(days=n)
    return (return_datetime.strftime("%Y-%m-%d"))

def load_data_colab():
#test_bucket43532
    print("Loading Data...")
    # client = storage.Client()
    # bucket = client.bucket("test_bucket43532")
    
    data_file = open('/content/drive/MyDrive/IE421/IEX_DATA/fp32/model_data/(300000,100,40)_0.000015/s_data_(300000, 100, 40).npy', 'rb')
    labels_file = open('/content/drive/MyDrive/IE421/IEX_DATA/fp32/model_data/(300000,100,40)_0.000015/s_labels_(300000, 3)_100.npy', 'rb')
    x = np.load(data_file, allow_pickle=True)
    x = np.expand_dims(x, axis=1)

    y = np.load(labels_file, allow_pickle=True)
    x_train = x[:10000]
    x_valid = x[200000 : 250000]
    x_test = x[250000 : 300000]

    del x

    y_train = y[:10000]
    y_valid = y[200000 : 250000]
    y_test = y[250000 : 300000]

    del y

    return x_train, x_valid, x_test, y_train, y_valid, y_test
def load_data_cloud():
    print("Loading Data From Cloud...")
    client = storage.Client()
    bucket = client.get_bucket("test_bucket43532")

    blob1 = bucket.blob("data_(300000, 100, 40).npy")
    with io.BytesIO() as in_memory_file:
        blob1.download_to_file(in_memory_file)
        in_memory_file.seek(0)
        x = np.load(in_memory_file, allow_pickle=True)
    x = np.expand_dims(x, axis=1)
                    
    blob2 = bucket.blob("labels_(300000, 100, 40).npy")
    with io.BytesIO() as in_memory_file2:
        blob2.download_to_file(in_memory_file2)
        in_memory_file2.seek(0)
        y = np.load(in_memory_file2, allow_pickle=True)

    x_train = x[:200000]
    x_valid = x[200000 : 250000]
    x_test = x[250000 : 300000]

    del x

    y_train = y[:200000]
    y_valid = y[200000 : 250000]
    y_test = y[250000 : 300000]

    del y

    print("Data Loaded")

    return x_train, x_valid, x_test, y_train, y_valid, y_test

def load_data_local():
    print("Loading Data From Disk...")
    curr = os.path.realpath(os.path.dirname(__file__))
    x = np.load(os.path.join(curr, 'data/intermediate_data/model_data/data_(300000, 100).npy'), allow_pickle=True)
    y = np.load(os.path.join(curr, 'data/intermediate_data/model_data/labels_(300000, 100).npy'), allow_pickle=True)
    x = np.expand_dims(x, axis=1)
  #  print(x.shape)
    x_train = x[:200000]
    x_valid = x[200000 : 250000]
    x_test = x[250000 : 300000]

    del x

    y_train = y[:200000]
    y_valid = y[200000 : 250000]
    y_test = y[250000 : 300000]

    del y

    print("Data Loaded")

    return x_train, x_valid, x_test, y_train, y_valid, y_test